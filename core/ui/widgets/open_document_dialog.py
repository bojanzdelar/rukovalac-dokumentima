
from PySide6.QtWidgets import QFileDialog

from core.service.file_system_service import FileSystemService

class OpenDocumentDialog(QFileDialog):
    def __init__(self, filters, parent=None):
        super().__init__(parent)
        
        self.setDirectory(FileSystemService.get_instance().get_root_directory())
            
        if not filters:
            filters = ["?"] # if there aren't any filters, hide all files
        else:
            filters = list(map(lambda x: f"*.{x}", filters))
        self.setNameFilters(filters)
        self.setFileMode(QFileDialog.ExistingFile)