from PySide6.QtCore import Signal
from PySide6.QtWidgets import QTabWidget


class TabWidget(QTabWidget):
    supported_types_changed = Signal(list)

    def __init__(self, parent):
        super().__init__(parent)

        self._supported_types = []
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.removeTab)
    
    @property
    def supported_types(self):
        return self._supported_types

    @supported_types.setter
    def supported_types(self, types):
        self._supported_types = types
        self.supported_types_changed.emit(types)