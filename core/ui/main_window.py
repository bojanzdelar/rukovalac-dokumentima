from PySide6.QtWidgets import QMainWindow
from PySide6.QtGui import QIcon

from core.ui.components.menu_bar import MenuBar
from core.ui.components.status_bar import StatusBar
from core.ui.components.tool_bar import Toolbar
from core.ui.widgets.tab_widget import TabWidget

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.resize(1280, 960)
        self.showMaximized()
        self.setWindowTitle("Document++")
        self.setWindowIcon(QIcon("core/resources/images/app-logo.png"))
        
        self._add_menu_bar()
        self._add_tool_bar()
        self._add_tab_widget()
        self._add_status_bar()   

    def _add_menu_bar(self):
        self.menu_bar = MenuBar(self)
        self.setMenuBar(self.menu_bar)

    def _add_tool_bar(self):
        self.tool_bar = Toolbar(self)
        self.addToolBar(self.tool_bar)

    def _add_tab_widget(self):
        self.tab_widget = TabWidget(self)
        self.setCentralWidget(self.tab_widget)

    def _add_status_bar(self):
        self.status_bar = StatusBar(self)
        self.setStatusBar(self.status_bar)