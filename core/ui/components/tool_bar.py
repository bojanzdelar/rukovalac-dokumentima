from collections.abc import Callable

from PySide6.QtCore import Signal
from PySide6.QtGui import QAction, QActionGroup
from PySide6.QtWidgets import QToolBar

from authentication.model.application_context import ApplicationContext
from core.util.util_functions import create_icon


class Toolbar(QToolBar):
    security_clicked_signal = Signal()

    def security_clicked(self):
        self.security_clicked_signal.emit()

    def __init__(self, parent):
        super(Toolbar, self).__init__(parent)

        self.setMovable(False)
        self.setAutoFillBackground(True)
        self.core_icons_path = "core/resources/icons/"

    def get_action(self, action_name: str) -> QAction:
        for action in self.actions():
            if action_name == action.text():
                return action

    def add_action(self, action_name: str, icon_path: str, function: Callable, key_press: str, tooltip: str, checkable: bool = False, group: QActionGroup = None) -> None:
        if self.get_action(action_name):
            return

        icon = create_icon(icon_path)
        action = QAction(icon, action_name, self)
        action.setShortcut(key_press)
        action.triggered.connect(function)
        action.setToolTip(tooltip)
        action.setCheckable(checkable)
        action.setActionGroup(group)
        self.addAction(action)

    def remove_action(self, action_name: str):
        self.removeAction(self.get_action(action_name))

    def add_undo_redo_actions(self, undo_action, redo_action):
        self.add_action("undo", self.core_icons_path + "undo-icon.png", undo_action, "", "Undo")
        self.add_action("redo", self.core_icons_path + "redo-icon.png", redo_action, "", "Redo")

    def add_administration_action(self):
        if ApplicationContext.get_user_data().permissions[0] == "ROLE_ADMIN":
            self.addSeparator()
            self.add_action("security", "authentication/resources/icons/security.png", self.security_clicked, "", "Administration")