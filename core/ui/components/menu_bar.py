from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QAction, QKeySequence
from PySide6.QtWidgets import QMenuBar, QMenu, QFileDialog, QMessageBox

from authentication.model.application_context import ApplicationContext
from authentication.ui.password_change_dialog import PasswordResetDialog
from core.ui.widgets.open_document_dialog import OpenDocumentDialog


class MenuBar(QMenuBar):
    file_open = Signal(str)
    close_all = Signal()
    sign_out = Signal()
    save_all = Signal()
    new_workspace = Signal(str) # FIXME: plugin should add this
    default_workspace = Signal()
    exit = Signal()
    undo = Signal()
    redo = Signal()
    display_plugin_manager = Signal()

    def __init__(self, parent):
        super().__init__(parent)
        self._actions = {
            "File": {
                "Open": QKeySequence(Qt.CTRL + Qt.Key_O),
                "Save all": QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_S),
                "Close all": QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_X),
                "Exit": QKeySequence(Qt.CTRL + Qt.Key_E)
            },
            "Edit": {
                "Undo": QKeySequence(Qt.CTRL + Qt.Key_Z),
                "Redo": QKeySequence(Qt.CTRL + Qt.Key_Y)
            },
            "Plugin": {
                "Plugin manager": QKeySequence(Qt.CTRL + Qt.Key_P)
            },
            "Help": {
                "Manual": QKeySequence(Qt.CTRL + Qt.Key_M),
                "About": QKeySequence(Qt.CTRL + Qt.Key_O)
            }
        }

        self._init_actions()
        self._add_user_menu()

    def add_action(self, menu_name, new_action, shortcut):
        """
        menu_name: the name of the menu to add to e.g. file_menu
        new_action: action name
        shortcut: e.g. QKeySequence(Qt.CTRL + Qt.Key_M)
        """
        # here we switch the string to be in this format e.g. file_menu > File
        name = menu_name[0].upper() + menu_name[1:]
        name = name.replace('_', ' ')
        name = name[0:name.rindex(' ')]

        self._actions[name][new_action] = shortcut
        action = QAction(new_action, self)
        action.setShortcut(shortcut)
        action.triggered.connect(lambda checked=False, text=new_action: self.action_event(text))
        self.__getattribute__(menu_name).addAction(action)


    def _init_actions(self):
        for key, value in self._actions.items():
            menu_name = key.lower() + "_menu"
            setattr(self, menu_name, self.addMenu(key))
            for key1, value1 in value.items():
                action = QAction(key1, self)
                action.setShortcut(value1)
                action.triggered.connect(lambda checked=False, text=key1: self.action_event(text))
                self.__getattribute__(menu_name).addAction(action)

    def action_event(self, command):
        """
        command: string - the name of the action
        """
        # File menu actions
        if command == "Open":
            dialog = OpenDocumentDialog(self.parent().tab_widget.supported_types)
            if dialog.exec():
                self.file_open.emit(dialog.selectedFiles()[0])
        # FIXME: plugin should add this
        elif command == "Open new workspace":
            folder_name = QFileDialog.getExistingDirectory()
            if len(folder_name) > 0:
                self.new_workspace.emit(folder_name)
        elif command == "Save all":
            self.save_all.emit()
        elif command == "Close all":
            self.close_all.emit()
        elif command == "Exit":
            self.exit.emit()
        elif command == "Default workspace":
            self.default_workspace.emit()

        # Edit menu actions
        elif command == "Undo":
            self.undo.emit()
        elif command == "Redo":
            self.redo.emit()
        # Plugin menu actions
        elif command == "Plugin manager":
            self.display_plugin_manager.emit()
        # Help menu actions
        elif command == "Manual":
            QMessageBox.information(self, "Manual",
                                              "You can the access the instructions for using the Document++ program on"
                                              + " the following <a href='https://www.documentplusplus.com'>link</a>")
        elif command == "About":
            QMessageBox.information(self, "About", "The Document++ program has been realized"
                                              + " as part of the project on the university subject Software Development Methodology.\n"
                                              + "\nAuthors are: \n1.Bojan Zdelar 2019/270983\n2.Drago Mazic 2019/270138\n3.Filip Zobic 2019/270036")

    def _add_user_menu(self):
        self.user_menu = self.addMenu(ApplicationContext.get_instance().user.username)

        # Sign out
        sign_out_action = QAction("Sign Out", self)
        # sign_out_action.triggered.connect(self.sign_out.emit)
        sign_out_action.triggered.connect(lambda: ApplicationContext.clear_instance())
        self.user_menu.addAction(sign_out_action)

        # Change password
        password_reset_action = QAction("Change Password", self)
        self.user_menu.addAction(password_reset_action) # retype and confirm form3 inputs all in all
        password_reset_action.triggered.connect(self.change_password)

    def change_password(self):
        form = PasswordResetDialog(self)
        form.show()

    def remove_action(self, menu_name: str, action_name: str):
        menu = self.__getattribute__(menu_name)
        for action in menu.actions():
            if action_name == action.text():
                menu.removeAction(action)
                self._actions[menu.title()][action] = {}

    def remove_actions(self):
        for key, value in self._actions.items():
            menu_name = key.lower() + "_menu"
            for action in self.__getattribute__(menu_name).actions():
                    self.__getattribute__(menu_name).removeAction(action)
                    self._actions[self.__getattribute__(menu_name).title()].pop(action.text())