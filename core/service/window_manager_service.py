from typing import Optional

from PySide6 import QtWidgets, QtCore

from authentication.auth_main_window import AuthMainWindow
from authentication.model.application_context import ApplicationContext
from authentication.model.application_context import ApplicationContext
from core.application_framework import ApplicationFramework


class WindowManagerService(QtCore.QObject):
    """
    On ApplicationContext change manager switches between login & main window
    We can expand upon this old context -> new context
    Currently not implemented since not needed
    """

    def __init__(self, app: QtWidgets.QApplication):
        super().__init__()
        self.app = app
        self.app_context: ApplicationContext = None
        app_context = ApplicationContext.get_instance()
        if app_context is None:
            self.main_auth_window = AuthMainWindow()
            self.main_auth_window.show()
        else:
            app_framework = ApplicationFramework()
            self.main_window = app_framework.mw
            self.main_window.show()

    def __create_main_window(self):
        context = ApplicationContext.get_instance()
        if context is None:
            raise Exception("Application context is not instantiated instantiate it before switching to Main Window")
        else:
            app_framework = ApplicationFramework()
            self.main_window = app_framework.mw
            if self.main_auth_window is not None:
                self.main_auth_window.hide()
                self.main_auth_window.close()
                self.main_auth_window = None
            self.main_window.show()

    def __create_login_window(self):
        context = ApplicationContext.get_instance()
        if context is not None:
            raise Exception("Application context is instantiated clear it first before navigating to login window")
        else:
            self.main_auth_window = AuthMainWindow()

            if self.main_window is not None:
                self.main_window.hide()
                self.main_window.close()
                self.main_window = None

            self.main_auth_window.show()

    def on_application_context_change(self, old_context: Optional[ApplicationContext]):
        new_context = ApplicationContext.get_instance()
        if old_context is None and new_context is not None:
            # transition from login to main window
            self.__create_main_window()
        elif old_context is not None and new_context is None:
            # transition from main window to login
            self.__create_login_window()
        else:
            raise Exception("Window transition error")