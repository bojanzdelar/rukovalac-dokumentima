from __future__ import annotations
import json
import os
import shutil
from pathlib import Path
from typing import Optional

from PySide6.QtWidgets import QDockWidget


class FileSystemService():

    _working_dir_path: str = "/workspace"

    __instance = None

    def __init__(self):
        if FileSystemService.__instance is not None:
            raise Exception("File system service is already instantiated")
        else:
            FileSystemService.__instance = self

    @staticmethod
    def get_instance() -> Optional[FileSystemService]:
        if FileSystemService.__instance is None:
            return FileSystemService.__instance
        else:
            return FileSystemService.__instance

    def get_workspace(self, possible_dir) -> [bool, str]:
        path_exists = os.path.exists(possible_dir)
        if not path_exists:
            return [False, self.get_root_directory()]
        is_dir = os.path.isdir(possible_dir)
        if not is_dir:
            return [False, self.get_root_directory()]

        return [True, possible_dir]

    def get_root_directory(self) -> str:
        return self._working_dir_path

    def create_workspace(self) -> None:
        if not self._working_dir_path.__contains__(Path.home().__str__()):
            self._working_dir_path = str(Path.home()) + self._working_dir_path
        directory_exists: bool = os.path.exists(self._working_dir_path)
        if not directory_exists:
            os.makedirs(self._working_dir_path)

    def change_dock_workspace(self, path: str, dock: QDockWidget) -> bool:
        directory_exists: bool = os.path.exists(path)
        if not directory_exists:
            os.makedirs(path)
        if os.path.isdir(path):
            file_system = dock.file_system_model
            file_system.setRootPath(path)
            dock.tree_view.setRootIndex(file_system.index(path))
            return True
        return False

    def create_new_file(self, new_file_name: str, root: str, folder_path: str or None, folder_name: str or None, new_file_type: str) -> None:
        if folder_path is None or folder_name is None:
            if not self._check_if_path_exists(root):
                self._create_new_directory(root)
            self.create_new_file_or_folder(new_file_name, new_file_type, root)
        else:
            if not self._check_if_path_exists(folder_path):
                self._create_new_directory(folder_path)

            if not os.path.isdir(folder_path):
                new_file_path = root + '/' + new_file_name
                if not os.path.exists(new_file_path):
                    if new_file_type == 'file':
                        open(new_file_path, 'a').close()
                    else:
                        os.makedirs(new_file_path)
                return
            self.create_new_file_or_folder(new_file_name, new_file_type, folder_path)

    def create_new_file_or_folder(self, new_file_name: str, new_file_type: str, root: str):
        try:
            new_file_path = root + '/' + new_file_name
            if not os.path.exists(new_file_path):
                if new_file_type == 'file':
                    open(new_file_path, 'a').close()
                else:
                    os.makedirs(new_file_path)
        except OSError:
            print('Failed creating the file')

    def update_file(self, updated_file_name: str, old_file_name: str, file_path: str) -> None:
        if file_path is not None:
            file_exists: bool = os.path.exists(file_path)
            if file_exists:
                index1 = file_path.rindex('/')
                part = file_path[index1 :]
                updated_file_path = file_path[: index1] + part.replace(old_file_name, updated_file_name)
                os.rename(file_path, updated_file_path)

    def delete_file(self, path: str or None) -> None:
        if path is not None:
            file_exists: bool = os.path.exists(path)
            if file_exists:
                try:
                    if self.check_if_path_is_directory(path):
                        shutil.rmtree(path)
                    else:
                        os.remove(path)
                except OSError:
                    print('Failed deleting the file')

    def _check_if_path_exists(self, path: str) -> bool:
        return os.path.exists(path)

    def check_if_path_is_directory(self, path) -> bool:
        return os.path.isdir(path)

    def _create_new_directory(self, path: str) -> None:
        os.makedirs(path)

    def save_json_file(self, file, path):
        with open(path, 'w') as f:
            json.dump(file, f, indent=2)
        f.close()

    def read_json_file(self, file_path):
        json_body = {}
        with open(file_path) as file:
            json_body = json.load(file)
        file.close()
        return json_body

