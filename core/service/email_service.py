from __future__ import annotations
import smtplib
from socket import gaierror
from typing import Optional


class EmailService():
    __instance = None
    _port = 1025
    _smtp_server = "127.0.0.1"
    _login = ""
    _password = ""
    _debug_email = True
    _sender = "localmachine@rukovalacdokumentima.com"

    def __init__(self):
        if EmailService.__instance is not None:
            raise Exception("Email service is already instantiated")
        else:
            EmailService.__instance = self

    @staticmethod
    def get_instance() -> Optional[EmailService]:
        if EmailService.__instance is None:
            return EmailService.__instance
        else:
            return EmailService.__instance

    def send_email(self, subject: str, body: str, receiver: str, sender = None) -> None:
        if sender is None:
            sender = EmailService._sender
        message = f"""From: {sender}\r\nTo: {receiver}\r\nSubject: {subject}\r\n\n{body}"""

        if EmailService._debug_email:
            try:
                with smtplib.SMTP(self._smtp_server, self._port) as server:
                    server.login(self._login, self._password)
                    rep = server.sendmail(sender, receiver, message)
                    server.close()
            except (gaierror, ConnectionRefusedError):
                print('Failed to connect to the server. Bad connection settings?')
            except smtplib.SMTPServerDisconnected as e:
                print('Failed to connect to the server. Wrong user/password?')
            except smtplib.SMTPException as e:
                print('SMTP error occurred: ' + str(e))


