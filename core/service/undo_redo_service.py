class UndoRedoStack:
    __pointer = -1
    __stack = []
    __history = 10
    __instance = None
    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
        return cls.__instance

    def push(self, command):
        for i in self.__stack:
            if command.doubleRotateEqual(i):
                return
                
        self.__stack.append(command)
        self.__pointer += 1
        if len(self.__stack) > self.__history:
            self.__stack.pop(0)
            self.__pointer -= 1

    def undo(self):
        if self.__pointer != -1:
            self.__stack[self.__pointer].undo()
            self.__pointer -= 1

    def redo(self):
        if len(self.__stack) > self.__pointer+1:
            self.__stack[self.__pointer+1].redo()
            self.__pointer += 1
    
    def clean(self):
        self.__pointer = -1
        self.__stack = []
    
    def save_history(self, number):
        self.__history = number

class Command:
    __redo = None
    __undo = None
    __param_redo = None
    __param_undo = None
    
    def __init__(self, redo, undo, param_redo = None, param_undo = None) -> None:
        """
        redo - function that is done\n
        undo - function to call to cancel the redo function\n
        param_redo - forward a tuple of parameters for redo function\n
        param_undo - forward a tuple of parameters for undo function\n

        """
        self.__redo = redo
        self.__undo = undo
        self.__param_redo = param_redo
        self.__param_undo = param_undo
    
    def redo(self):
        self.__redo(*self.__param_redo)
    
    def undo(self):
        self.__undo(*self.__param_undo)
        
    def doubleRotateEqual(self, other):
        """
        rotate methods self.__redo and self.__undo
        """
        b1 = hash((self.__redo, self.__undo, self.__param_redo, self.__param_undo)) == hash((other.__undo, other.__redo, other.__param_undo, other.__param_redo))
        # b2 = hash(self) == hash(other)

        return b1
    
    def __eq__(self, o: object) -> bool:
        return hash(self) == hash(o)

    def __hash__(self) -> int:
        return hash((self.__redo, self.__undo, self.__param_redo, self.__param_undo))