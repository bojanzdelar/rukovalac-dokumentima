from abc import ABC, abstractmethod


class AbstractHandler(ABC):
    def __init__(self, path, format=None):
        self.path = path
        self.format = format
        self.data = None

    @abstractmethod
    def read_file(self):
        ...

    @abstractmethod
    def save_file(self):
        ...