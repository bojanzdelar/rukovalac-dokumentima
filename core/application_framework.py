from PySide6.QtCore import Qt
from PySide6.QtGui import QMouseEvent, QKeyEvent

from core.ui.main_window import MainWindow
from core.service.undo_redo_service import UndoRedoStack
from authentication.service.admin_service import AdministrationService
from plugin_manager.plugin_manager import PluginManager

from core.interface.workspace_explorer_out import WorkspaceExplorerOut
from core.interface.document_editor_out import DocumentEditorOut
from core.interface.page_editor_out import PageEditorOut
from core.interface.element_editor_out import ElementEditorOut
from plugin_manager.interface.plugin_manager_in import PluginManagerIn
from plugins.workspace_explorer.interface.workspace_explorer_in import WorkspaceExplorerIn
from plugins.document_editor.interface.document_editor_in import DocumentEditorIn
from plugins.page_editor.interface.page_editor_in import PageEditorIn
from plugins.page_editor.interface.element_editor_in import ElementEditorIn

from typing import Dict, List
from plugins.page_editor.ui.element_view import ElementView
from plugins.page_editor.model.page_handler import PageHandler
from plugins.page_editor.ui.page_view import PageView


class ApplicationFramework(WorkspaceExplorerOut, DocumentEditorOut, PageEditorOut, ElementEditorOut):
    def __init__(self):
        self.undo_redo_stack = UndoRedoStack()

        self._init_main_window()
        self._connect_to_signals()

        self.workspace_explorer: WorkspaceExplorerIn = None
        self.document_editor: DocumentEditorIn = None
        self.page_editor: PageEditorIn = None
        self.element_editors: Dict[ElementEditorIn] = {}
        
        self.plugin_manager: PluginManagerIn = PluginManager(self)
        self.mw.menu_bar.display_plugin_manager.connect(lambda: self.plugin_manager.show_dialog(self.mw))

    def _init_main_window(self):
        self.mw = MainWindow()
        self.mw.tool_bar.add_undo_redo_actions(self.undo_redo_stack.undo, self.undo_redo_stack.redo)
        self.mw.tool_bar.add_administration_action()
        self.mw.show()

    def _connect_to_signals(self):
        self.mw.menu_bar.file_open.connect(self.open_document)        
        self.mw.menu_bar.close_all.connect(self.mw.tab_widget.clear)
        self.mw.menu_bar.exit.connect(self.mw.close)
        self.mw.menu_bar.undo.connect(self.undo_redo_stack.undo)
        self.mw.menu_bar.redo.connect(self.undo_redo_stack.redo)
        self.mw.tool_bar.security_clicked_signal.connect(self.open_administration)
        self.mw.tab_widget.currentChanged.connect(self.uncheck_element_actions)
        self.mw.tab_widget.supported_types_changed.connect(self.change_supported_types)

    def push_command(self, command):
        self.undo_redo_stack.push(command)

    def open_administration(self):
        service = AdministrationService(self.mw)
        service.dialog.exec()

    ##################################################################################################
    # WorkspaceExplorerOut
    ##################################################################################################

    def add_workspace_explorer(self, workspace_explorer: WorkspaceExplorerIn, actions: List) -> None:
        self.workspace_explorer = workspace_explorer
        self.change_supported_types(self.mw.tab_widget.supported_types)
        self.mw.addDockWidget(Qt.LeftDockWidgetArea, workspace_explorer)
        for action in actions:
            self.mw.menu_bar.add_action(*action)

        self.mw.menu_bar.new_workspace.connect(self.workspace_explorer.change_workspace)
        self.mw.menu_bar.default_workspace.connect(self.workspace_explorer.change_to_default_workspace)

    def remove_workspace_explorer(self, actions: List) -> None:
        self.mw.removeDockWidget(self.workspace_explorer)
        self.workspace_explorer = None
        for action in actions:
            self.mw.menu_bar.remove_action(action[0], action[1])

    def get_supported_types(self) -> List:
        return self.mw.tab_widget.supported_types

    def open_document(self, path: str) -> None:
        data = self.document_editor.open_document(path)
        if not data:
            return

        tab_name, id, widget = data

        index = self.mw.tab_widget.addTab(widget, tab_name)
        self.mw.tab_widget.setCurrentIndex(index)
        self.mw.tab_widget.setTabWhatsThis(index, id)
        self.mw.tab_widget.currentWidget().setFocus()

        for element_editor in self.element_editors.values():
            element_editor.add_action()

    ##################################################################################################
    # call WorkspaceExplorerIn
    ##################################################################################################

    def change_supported_types(self, types: List) -> None:
        if self.workspace_explorer:
            self.workspace_explorer.change_supported_types(types)

    ##################################################################################################
    # PageEditorOut
    ##################################################################################################

    def add_page_editor(self, page_editor: PageEditorIn) -> None:
        self.page_editor = page_editor

    def remove_page_editor(self) -> None:
        self.page_editor = None

    def get_current_page(self) -> PageView:
        return self.document_editor.get_current_page()

    def create_element(self, type: str, path: str, parent_path: str) -> ElementView:
        if type not in self.element_editors:
            return
        
        return self.element_editors[type].create_element(path, parent_path)


    def clone_element(self, element: ElementView):
        # FIXME: this attribute shouldn't be accessed directly like this
        # pazi mozda si u pagescene i jos negde radio iste ovakve stvari
        if element.data_type not in self.element_editors:
            return
        
        return self.element_editors[element.data_type].clone_element(element)

    def save_element(self, type: str, element: ElementView, path: str, document_path: str, existing_paths: list) -> str:
        if type not in self.element_editors:
            return
        
        return self.element_editors[type].save_element(element, path, document_path, existing_paths)

    def delete_element(self, type: str, parent: str, path: str):
        if type not in self.element_editors:
            return

        self.element_editors[type].delete_element(parent, path)

    def signalize_page_clicked(self, mouse_event: QMouseEvent):
        for element_editor in self.element_editors.values():
            element_editor.insert_element(mouse_event)
 
    def signalize_scroll(self, event: QKeyEvent):
        self.document_editor.scroll_page(event)

    ##################################################################################################
    # ElementEditorOut
    ##################################################################################################

    def add_element_editor(self, type, editor: ElementEditorIn) -> None:
        self.element_editors[type] = editor
        
        if self.document_editor:
            self.document_editor.update_views()
    
    def remove_element_editor(self, type) -> None:
        if type in self.element_editors:
             del self.element_editors[type]
        
        if self.document_editor:
            self.document_editor.update_views()

    def add_document_action(self, name, icon, func, key_press, tooltip):
        if self.document_editor:  
            self.document_editor.add_element_action(name, icon, func, key_press, tooltip)

    def remove_document_action(self, name):
        if self.document_editor:
            self.document_editor.remove_element_action(name)

    def is_action_checked(self, name):
        return self.document_editor.is_action_checked(name)

    def set_action_checked(self, name, checked: bool):
        self.document_editor.set_action_checked(name, checked)

    def request_insert_element(self, item: ElementView, x: float, y: float) -> bool:
        return self.page_editor.add_element(item, x, y)   

    ##################################################################################################
    # DocumentEditorOut
    ##################################################################################################

    def add_document_editor(self, document_editor: DocumentEditorIn, supported_types: List):
        self.document_editor = document_editor
        self.mw.tab_widget.supported_types = supported_types

    def remove_document_editor(self):
        self.document_editor = None
        self.mw.tab_widget.supported_types = []
        self.mw.tab_widget.clear()

    def get_current_document(self):
        return self.mw.tab_widget.currentWidget()

    def get_all_documents(self) -> List:
        return [self.mw.tab_widget.widget(i) for i in range(self.mw.tab_widget.count())]

    def set_current_document(self, name) -> bool:
        for i in range(0, self.mw.tab_widget.count()):
            if self.mw.tab_widget.tabWhatsThis(i) == name:
                self.mw.tab_widget.setCurrentIndex(i)
                return True
        return False
    
    def create_page_handler(self, path, parent_path, format="PG") -> PageHandler:
        return self.page_editor.create_handler(path, parent_path, format)

    def create_page_view(self, handler, parent=None, central_wgt=False, gen_view_scene=None) -> PageView:
        return self.page_editor.create_view(handler, parent, central_wgt, gen_view_scene)

    ##################################################################################################
    # call DocumentEditorIn
    ##################################################################################################

    def uncheck_element_actions(self):
        if self.document_editor:
            self.document_editor.uncheck_element_actions()