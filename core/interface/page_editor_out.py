from abc import ABC, abstractmethod
from PySide6.QtGui import QMouseEvent, QKeyEvent
from plugins.page_editor.interface.page_editor_in import PageEditorIn
from plugins.page_editor.ui.element_view import ElementView
from plugins.page_editor.ui.page_view import PageView

class PageEditorOut(ABC):
    @abstractmethod
    def add_page_editor(self, page_editor: PageEditorIn) -> None:
        ...

    @abstractmethod
    def remove_page_editor(self) -> None:
        ...

    @abstractmethod
    def get_current_page(self) -> PageView:
        ...

    @abstractmethod
    def create_element(self, type: str, path: str, parent_path: str) -> ElementView:
        ...

    @abstractmethod
    def clone_element(self, element: ElementView):
        ...

    @abstractmethod
    def save_element(self, type: str, element: ElementView, path: str, document_path: str, existing_paths: list) -> str:
        ...

    @abstractmethod
    def delete_element(self, type: str, parent: str, path: str):
        ...

    @abstractmethod
    def signalize_page_clicked(self, mouse_event: QMouseEvent):
        ...

    @abstractmethod
    def signalize_scroll(self, event: QKeyEvent):
        ...