from abc import ABC, abstractmethod
from xmlrpc.client import Boolean
from plugins.page_editor.interface.element_editor_in import ElementEditorIn
from plugins.page_editor.ui.element_view import ElementView

class ElementEditorOut(ABC):
    @abstractmethod
    def add_element_editor(self, editor: ElementEditorIn) -> None:
        ...

    @abstractmethod
    def remove_element_editor(self, editor: ElementEditorIn) -> None:
        ...

    @abstractmethod
    def add_document_action(self, name, icon, func, key_press, tooltip):
        ...

    @abstractmethod
    def remove_document_action(self, name):
        ...

    @abstractmethod
    def is_action_checked(self, name): 
        ...

    @abstractmethod
    def set_action_checked(self, name, checked: bool):
        ...

    @abstractmethod
    def request_insert_element(self, item: ElementView, x: float, y: float, scale: float) -> Boolean:
        ...    
