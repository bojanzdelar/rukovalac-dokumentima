from abc import ABC, abstractmethod
from typing import List
from plugins.workspace_explorer.interface.workspace_explorer_in import WorkspaceExplorerIn

class WorkspaceExplorerOut(ABC):
    @abstractmethod
    def add_workspace_explorer(self, workspace_explorer: WorkspaceExplorerIn, actions: List) -> None:
        ...

    @abstractmethod
    def remove_workspace_explorer(self, actions: List) -> None:
        ...

    @abstractmethod
    def get_supported_types(self) -> List:
        ...

    @abstractmethod
    def open_document(self, path: str) -> None:
        ...    