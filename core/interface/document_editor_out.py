from abc import ABC, abstractmethod
from typing import List
from plugins.document_editor.interface.document_editor_in import DocumentEditorIn
from plugins.document_editor.ui.document_widget import DocumentWidget

class DocumentEditorOut(ABC):
    @abstractmethod
    def add_document_editor(self, document_editor: DocumentEditorIn):
        ...

    @abstractmethod
    def remove_document_editor(self):
        ...

    @abstractmethod
    def get_current_document(self) -> DocumentWidget:
        ...

    @abstractmethod
    def get_all_documents(self) -> List:
        ...

    # TODO: this should be replaced 
    @abstractmethod
    def create_page_handler(self, path, parent_path, format="PG"):
        ...

    # TODO: this should be replaced
    @abstractmethod
    def create_page_view(self, handler, parent=None, central_wgt=False, gen_view_scene=None):
        ...

    @abstractmethod
    def create_element(self, type: str, path: str, parent_path: str):
        ...

    @abstractmethod
    def clone_element(self, element: object):
        ...