from pathlib import Path
from PySide6.QtGui import QIcon

def create_icon(file_path) -> QIcon:
    return QIcon(file_path)

def get_file_name(file_path):
    file_name = file_path.split("/")[-1]
    return file_name

def get_file_extension(file_path):
    extension = file_path.split(".")[-1]
    return extension

def get_file_path_without_extension(file_path):
    extension_index = file_path.rfind(".")
    return file_path[:extension_index]

def get_folder_path(file_path):
    path = Path(file_path)
    return path.parent

def remove_trailing_whitespace(string: str) -> str or None:
    striped_string = string.strip()
    return striped_string if (len(striped_string) != 0) else None

# Some classes e.g. QDockWidget and Extension have different metaclasses
# so we have to wrap them
def new_meta_class(list_classes):
    """
    :param list_classes: e.g. [QDockWidget, Extension] list of classes to inherit
    """
    list1 = []
    name = ""
    for i in list_classes:
        list1.append(type(i))
        name += str(i.__name__)
        
    return type(name,(*list1,),{})