from __future__ import annotations
import hashlib
import os
import re
import uuid
import urllib.request
from pathlib import Path

from typing import List, Optional

from authentication.model.application_context import ApplicationContextSignalManager, \
    ApplicationContext
from authentication.model.user import User
from authentication.model.user_builder import UserBuilder
from core.service.email_service import EmailService
from core.service.file_system_service import FileSystemService


class AuthService():
    __instance = None

    _email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    _number_regex = r'\d+'
    _users_path = str(Path.home()) + "/.rukovalacDokumentima/"
    _file_name = "users.json"
    _ENCODING = 'utf-8'

    users: List[User] = []

    def __init__(self):
        if AuthService.__instance is not None:
            raise Exception("Auth service is already instantiated")
        else:
            self.email_service: EmailService = None
            self.file_system_service: FileSystemService = None

            email_service = EmailService.get_instance()
            if email_service is None:
                email_service = EmailService()
            self.email_service = email_service
            file_system_service = FileSystemService.get_instance()
            if file_system_service is None:
                file_system_service = FileSystemService()
            self.file_system_service = file_system_service
            AuthService.__instance = self
            self.load_users()
        ApplicationContextSignalManager.get_instance().user_updated_emitter.connect(self.replace_user)

    @staticmethod
    def get_instance() -> Optional[AuthService]:
        if AuthService.__instance is None:
            return AuthService.__instance
        else:
            return AuthService.__instance

    def register_user_event(self, user: dict) -> [bool, str, Optional[User]]:
        # Field Validation
        if self.is_text_empty(user['username']):
            return [False, "Username is required", None]

        if self.is_text_empty(user['user_number']):
            return [False, "User number is required", None]

        if not re.fullmatch(self._number_regex, user['user_number']):
            return [False, "User number pattern must be all in digits", None]

        if not re.fullmatch(self._email_regex, user['username']):
            return [False, "Username/email does not match pattern johndoe@gmail.com", None]

        if self.is_text_empty(user['password']):
            return [False, "Password is required", None]

        # load users file again and check if username taken
        self.load_users()
        if self.user_exists(user['username']):
            return [False, "Username is taken", None]

        if self.user_exists_by_user_number(user['user_number']):
            return [False, "User number is taken", None]

        # Hashing and creating

        root = Path.home().__str__() + self.file_system_service.get_root_directory()
        new_working_dir_path = root + '/' + user["username"]
        self.file_system_service.create_new_file_or_folder(user["username"], "folder", root)

        new_user = UserBuilder.builder().username(user["username"]).id(uuid.uuid4().__str__()).permissions(
            user["roles"]).encrypted_password(self.hash_password(user['password']).decode("latin-1")).last_working_dir(
            new_working_dir_path).user_number(user["user_number"]).enabled(True).build()

        self.users.append(new_user)

        # Save file
        self.save_users()

        return [True, "Registration completed successfully", new_user]

    def login_user_event(self, user: dict) -> [bool, str, Optional[User]]:
        if self.is_text_empty(user['username']) and self.is_text_empty(user['username']):
            return [False, "User number is required", None]

        if not re.fullmatch(self._number_regex, user['username']):
            return [False, "User number must be only digits", None]

        if self.is_text_empty(user['password']):
            return [False, "Password is required", None]

        self.load_users()
        json_user = self.get_user_by_number(user['username'])
        if json_user is None:
            return [False, "User does not exist", None]
        # password
        json_password = json_user.encrypted_password.encode("latin-1")
        salt = self.get_salt(json_password)
        json_password_hash = self.get_hash(json_password)

        user_form_password = user['password']
        user_form_password_hashed = self.get_hash(self.hash_password(user_form_password, salt))
        if user_form_password_hashed == json_password_hash:
            if not json_user.enabled:
                return [False, "User has been disabled by an administrator", None]
            return [True, "User authenticated successfully", json_user]
        else:
            if json_user.temp_password is not None:
                json_password = json_user.temp_password.encode("latin-1")
                salt = self.get_salt(json_password)
                json_password_hash = self.get_hash(json_password)
                user_form_password_hashed = self.get_hash(self.hash_password(user_form_password, salt))
                if user_form_password_hashed == json_password_hash:
                    json_user.encrypted_password = json_user.temp_password
                    json_user.temp_password = None
                    self.save_users()
                    return [True,
                            "User authenticated successfully with temporary password which has overwritten main password",
                            json_user]
            return [False, "Wrong password", None]

    def forgot_password_event(self, user_number: str) -> [bool, str]:
        if self.is_text_empty(user_number) is None:
            return [False, "User number is required"]
        if not re.fullmatch(self._number_regex, user_number):
            return [False, "User number must be digits only"]
        self.load_users()
        json_user = self.get_user_by_number(user_number)
        if json_user is None:
            return [False, "User does not exist"]
        if json_user.temp_password is not None:
            return [False, "Password reset email has already been sent"]
        if not json_user.enabled:
            return [False, "User has been disabled by an administrator"]

        if not self.connection_to_internet_exists():
            return [False, "Can't send email no internet access"]

        random_string = uuid.uuid4().__str__()
        self.send_password_reset_email(json_user.username, random_string)
        hashed_temp_password = self.hash_password(random_string)
        json_user.temp_password = hashed_temp_password.decode("latin-1")
        print(random_string)
        self.save_users()

        return [True, "Temporary password sent to email"]

    def change_password_event(self, form_data) -> [bool, str]:
        current_password = form_data['current_password']
        new_password = form_data['new_password']
        confirm_password = form_data['confirm_password']

        if self.is_text_empty(new_password):
            return [False, "New password is required"]
        if self.is_text_empty(current_password):
            return [False, "Current password is required"]
        if self.is_text_empty(confirm_password):
            return [False, "Confirm password is required"]

        if new_password != confirm_password:
            return [False, "New passwords don't match"]
        self.load_users()

        json_user = ApplicationContext.get_instance().get_user_data()
        # password
        json_password = json_user.encrypted_password.encode("latin-1")
        salt = self.get_salt(json_password)
        json_password_hash = self.get_hash(json_password)

        user_form_password_hashed = self.get_hash(self.hash_password(current_password, salt))
        if user_form_password_hashed != json_password_hash:
            return [False, "Current password is wrong"]

        json_user.encrypted_password = self.hash_password(new_password).decode("latin-1")
        ApplicationContext.get_instance().update_user(json_user)
        self.replace_user(json_user)
        return [True, "Password successfully updated"]

    def admin_modify_user(self, form: dict, user: User) -> [bool, str]:
        if self.is_text_empty(form['username']):
            return [False, "Username is required"]

        if self.is_text_empty(form['user_number']):
            return [False, "User number is required"]

        if not re.fullmatch(self._number_regex, form['user_number']):
            return [False, "User number pattern must be all in digits"]

        if not re.fullmatch(self._email_regex, form['username']):
            return [False, "Username/email does not match pattern johndoe@gmail.com"]

        self.load_users()
        user_from_db = self.get_user_by_username(form['username'])
        if user_from_db is not None and user_from_db.id != user.id:
            return [False, "Username is taken"]

        user_from_db = self.get_user_by_number(form['user_number'])
        if user_from_db is not None and user_from_db.id != user.id:
            return [False, "User number is taken"]

        if form['should_change_password']:
            if self.is_text_empty(form['password']):
                return [False, "Password is required"]
            else:
                hashed_password = self.hash_password(form['password']).decode("latin-1")
                user.temp_password = None
                user.encrypted_password = hashed_password
        user.username = form["username"]
        user.user_number = form["user_number"]
        user.enabled = form["enabled"]
        user.permissions = form["roles"]
        self.replace_user(user)
        return [True, "User has been modified"]

    def replace_user(self, json_user: User):
        # Iterator pattern
        self.load_users()
        for i, value in enumerate(self.users):
            if json_user.id == self.users[i].id:
                self.users[i] = json_user
                self.save_users()
                return
        raise Exception("User not found")

    def load_users(self):
        directory_exists: bool = os.path.exists(self._users_path)
        if not directory_exists:
            self.file_system_service.create_new_file_or_folder("", "folder", self._users_path)

        file_path = self._users_path + self._file_name

        file_exists: bool = os.path.exists(file_path)

        if not file_exists:
            root = Path.home().__str__() + self.file_system_service.get_root_directory()
            new_working_dir_path = root + '/' + "admin@gmail.com"
            self.file_system_service.create_new_file_or_folder("admin@gmail.com", "folder", root)

            new_user = UserBuilder.builder().username("admin@gmail.com").id(uuid.uuid4().__str__()).permissions(
                ["ROLE_USER"]).encrypted_password(self.hash_password('admin').decode("latin-1")).last_working_dir(
                new_working_dir_path).user_number("001").enabled(True).build()
            self.users.append(new_user)
            self.save_users()
        else:
            nu_users_loaded = 0
            json_file = self.file_system_service.read_json_file(file_path)
            users: List[User] = []
            for usr in json_file["users"]:
                loaded_usr = UserBuilder.builder().username(usr["username"]).id(usr["id"]).permissions(
                    usr["permissions"]).encrypted_password(usr['encrypted_password']).last_working_dir(
                    usr['last_working_dir']).user_number(usr["user_number"]).enabled(usr["enabled"]).plugins(
                    usr['plugins']).build()

                if "temp_password" in usr:
                    loaded_usr.temp_password = usr["temp_password"]
                users.append(loaded_usr)
                nu_users_loaded += 1
            self.users = users

    def save_users(self):
        directory_exists: bool = os.path.exists(self._users_path)
        if not directory_exists:
            self.file_system_service.create_new_file_or_folder("", "folder", self._users_path)

        file_path = self._users_path + self._file_name
        json_file = {}
        json_file["users"] = []
        for usr in self.users:
            json_user = {
                "username": usr.username,
                "id": usr.id,
                "permissions": usr.permissions,
                "encrypted_password": usr.encrypted_password,
                "last_working_dir": usr.last_working_dir,
                "user_number": usr.user_number,
                "enabled": usr.enabled,
                "plugins": usr.plugins
            }
            if usr.temp_password is not None:
                json_user["temp_password"] = usr.temp_password
            json_file["users"].append(json_user)
        self.file_system_service.save_json_file(json_file, file_path)

    def user_exists(self, username: str) -> bool:
        for u in self.users:
            if u.username == username:
                return True
        return False

    def get_user_by_username(self, username: str) -> Optional[User]:
        for u in self.users:
            if u.username == username:
                return u
        return None

    def get_user_by_number(self, number: str) -> Optional[User]:
        for u in self.users:
            if u.user_number == number:
                return u
        return None

    def user_exists_by_user_number(self, user_number):
        for u in self.users:
            if u.user_number == user_number:
                return True
        return False

    def get_user_by_username_or_id(self, identifier):
        for u in self.users:
            if u.username == identifier or u.user_number == identifier:
                return u
        return None

    def send_password_reset_email(self, email: str, temp_password: str):
        body = "Rukovalac Dokumentima \n\nA temporary password has been created for your username on Singidunum desktop: \n\nNew password: {} \n\nOn login with new password old password will be replaced \n\nIf you did not send this password reset request contact your corosponding IT department".format(
            temp_password)
        self.email_service.send_email("Password Reset", body, email)

    @staticmethod
    def is_text_empty(text: str):
        return len(text.strip()) == 0

    @staticmethod
    def hash_password(password: str, salt=None) -> bytes:
        if salt is None:
            salt = os.urandom(32)
        key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)
        storage = salt + key
        return storage

    @staticmethod
    def get_salt(hashed_password: bytes) -> bytes:
        return hashed_password[:32]

    @staticmethod
    def get_hash(hashed_password: bytes) -> bytes:
        return hashed_password[32:]

    @staticmethod
    def connection_to_internet_exists(host='http://google.com'):
        try:
            urllib.request.urlopen(host)
            return True
        except:
            return False

