from PySide6.QtWidgets import QMainWindow

from authentication.service.auth_service import AuthService
from authentication.service.auth_service import AuthService
from authentication.ui.administration_dialog import AdministrationDialog


class AdministrationService:
    def __init__(self, parent: QMainWindow):
        # Auth service
        self.auth_service: AuthService = None
        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service
        # Dialog
        self.dialog = AdministrationDialog(self, self.auth_service.users, parent)


