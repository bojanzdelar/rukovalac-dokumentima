from __future__ import annotations
from typing import List
from authentication.model.user import User

class UserBuilder():
    def __init__(self):
        self._id: str = None
        self._encrypted_password: str = None
        self._temp_password: str = None
        self._username: str = None
        self._last_working_dir: str = None
        self._user_number: str = None
        self._permissions: List[str] = None
        self._plugins: dict = {
                "workspace-explorer": True,
                "document-editor": True,
                "page-editor": True,
                "text-editor": True,
                "image-viewer": True
            }
        self._enabled: bool = None

    def id(self, _id: str) -> UserBuilder:
        self._id = _id
        return self

    def encrypted_password(self, password: str) -> UserBuilder:
        self._encrypted_password = password
        return self

    def temp_password(self, temp_password: str) -> UserBuilder:
        self._temp_password = temp_password
        return self

    def username(self, username: str) -> UserBuilder:
        self._username = username
        return self

    def last_working_dir(self, directory: str) -> UserBuilder:
        self._last_working_dir = directory
        return self

    def user_number(self, user_number: str) -> UserBuilder:
        self._user_number = user_number
        return self

    def permissions(self, permissions: List[str]) -> UserBuilder:
        self._permissions = permissions
        return self

    def plugins(self, plugins: dict) -> UserBuilder:
        self._plugins = plugins
        return self

    def enabled(self, enabled: bool) -> UserBuilder:
        self._enabled = enabled
        return self

    @staticmethod
    def builder() -> UserBuilder:
        return UserBuilder()

    def build(self) -> User:
        return User(self._id,
                    self._username,
                    self._user_number,
                    self._encrypted_password,
                    self._temp_password,
                    self._last_working_dir,
                    self._permissions,
                    self._plugins,
                    self._enabled)

