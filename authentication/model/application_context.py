from __future__ import annotations
from typing import Optional

from PySide6 import QtCore
from PySide6.QtCore import Signal

from authentication.model.user import User


class ApplicationContext():

    __instance = None

    def __init__(self, user: User):
        if ApplicationContext.__instance is not None:
            raise Exception("Application context is already instantiated")
        else:
            old_context = ApplicationContext.__instance
            self.user = user
            ApplicationContext.__instance = self
            ApplicationContextSignalManager.get_instance().context_changed_handler(old_context)

    @staticmethod
    def get_instance() -> Optional[ApplicationContext]:
        if ApplicationContext.__instance is None:
            return ApplicationContext.__instance
        else:
            return ApplicationContext.__instance

    @staticmethod
    def clear_instance() -> None:
        old_context = ApplicationContext.__instance
        ApplicationContext.__instance = None
        ApplicationContextSignalManager.get_instance().context_changed_handler(old_context)

    @staticmethod
    def get_user_data() -> User:
        return ApplicationContext.__instance.user

    @staticmethod
    def update_user(user: any) -> None:
        ApplicationContextSignalManager.get_instance().user_updated_handler(user)
        ApplicationContext.get_instance().user = user

class ApplicationContextSignalManager(QtCore.QObject):

    context_change_emitter = Signal(ApplicationContext)
    user_updated_emitter = Signal(User)
    __instance = None

    def __init__(self):
        if ApplicationContextSignalManager.__instance is not None:
            raise Exception("Application context is already instantiated")
        else:
            super().__init__()

    def context_changed_handler(self, context):
        self.context_change_emitter.emit(context)

    def user_updated_handler(self, user: any):
        self.user_updated_emitter.emit(user)

    @staticmethod
    def get_instance():
        """
        :return:
        ApplicationContext||None
        """
        if ApplicationContextSignalManager.__instance is None:
            ApplicationContextSignalManager.__instance = ApplicationContextSignalManager()
        return ApplicationContextSignalManager.__instance


