from typing import Optional, List


class User:
    def __init__(self, id: str, username: str, user_number: str, encrypted_password: str,
                 temp_password: Optional[str], last_working_dir: str,
                 permissions: List[str], plugins: dict, enabled: bool):
        self.id = id
        self.username = username
        self.encrypted_password = encrypted_password
        self.user_number = user_number
        self.last_working_dir = last_working_dir
        self.enabled = enabled
        self.permissions = permissions
        self.plugins = plugins
        self.temp_password = temp_password

