from PySide6 import QtCore
from PySide6.QtCore import Signal, Qt
from PySide6.QtGui import QKeySequence
from PySide6.QtWidgets import QWidget, QHBoxLayout, QLabel, QLineEdit, QPushButton, QToolButton, QStyle, QComboBox


class SearchBoxWidget(QWidget):
    signal = Signal(dict)

    def __init__(self, parent, info: dict):
        super().__init__(parent)

        self.setMaximumWidth(800)
        self.searchBoxLayout = QHBoxLayout()
        self.setLayout(self.searchBoxLayout)
        email_label = QLabel("Email/Number")
        email_label.setMaximumWidth(120)
        email_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.searchBoxLayout.addWidget(email_label)
        self.username_and_number_search_text = QLineEdit()
        self.username_and_number_search_text.setMaximumWidth(350)
        self.searchBoxLayout.addWidget(self.username_and_number_search_text)
        self.submit_search_btn = QPushButton("Search")
        self.setMaximumHeight(100)
        self.submit_search_btn.setMaximumWidth(200)
        self.searchBoxLayout.addWidget(self.submit_search_btn)
        # self.setStyleSheet('background-color: green;')
        # P A G # # #
        self.button1 = QToolButton(self)
        self.button1.setShortcut(QKeySequence(Qt.Key_Down))
        self.button1.setIcon(self.button1.style().standardIcon(QStyle.SP_MediaSeekBackward))
        self.button1.clicked.connect(self.first_page_handler)
        self.searchBoxLayout.addWidget(self.button1)

        self.button2 = QToolButton(self)
        self.button2.setShortcut(QKeySequence(Qt.Key_Left))
        self.button2.setArrowType(QtCore.Qt.LeftArrow)
        self.button2.clicked.connect(self.previous_page_handler)
        self.searchBoxLayout.addWidget(self.button2)

        self.page_display = QLabel("1/1")
        # self.page_display.setStyleSheet('background-color: purple;')
        self.page_display.setMaximumWidth(30)
        self.page_display.setAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.searchBoxLayout.addWidget(self.page_display)

        self.button3 = QToolButton(self)
        self.button3.setShortcut(QKeySequence(Qt.Key_Right))
        self.button3.setArrowType(QtCore.Qt.RightArrow)
        self.button3.clicked.connect(self.next_page_handler)
        self.searchBoxLayout.addWidget(self.button3)

        self.button4 = QToolButton(self)
        self.button4.setShortcut(QKeySequence(Qt.Key_Up))
        self.button4.setIcon(self.button1.style().standardIcon(QStyle.SP_MediaSeekForward))
        self.button4.clicked.connect(self.last_page_handler)
        self.searchBoxLayout.addWidget(self.button4)

        self.cb = QComboBox()
        self.cb.addItems(["20", "10", "5"])
        self.searchBoxLayout.addWidget(self.cb)
        self.cb.currentIndexChanged.connect(self.selectionchange_handler)

        self.items_per_page = 20
        self.current_page = 0
        self.number_of_pages = 0
        self.on_last_page = True
        self.on_first_page = True
        self.search_string = ""
        self.refresh_info(info)
        self.submit_search_btn.clicked.connect(self.search_handler)
        self.username_and_number_search_text.returnPressed.connect(self.submit_search_btn.click)

    def refresh_info(self, info: dict):
        self.current_page = info['current_page']
        self.number_of_pages = info['number_of_pages']
        self.on_last_page = self.current_page == self.number_of_pages
        self.on_first_page = self.current_page == 0
        self.search_string = info['search_string']
        self._refresh_ui()

    def _refresh_ui(self):
        self.page_display.setText(str((self.current_page+1))+"/"+str(self.number_of_pages+1))
        self.button1.setDisabled(self.on_first_page)
        self.button2.setDisabled(self.on_first_page)
        self.button3.setDisabled(self.on_last_page)
        self.button4.setDisabled(self.on_last_page)

    def generate_params(self) -> dict:
        params = {
            "current_page": self.current_page,
            "search_string": self.search_string,
            "items_per_page": self.items_per_page
        }
        return params

    def selectionchange_handler(self, i):
        selected = self.cb.currentText()
        if selected == "20":
            self.items_per_page = 20
        elif selected == "10":
            self.items_per_page = 10
        elif selected == "5":
            self.items_per_page = 5
        else:
            return
        self.current_page = 0
        self.signal.emit(self.generate_params())

    def search_handler(self):
        self.search_string = self.username_and_number_search_text.text()
        self.current_page = 0
        self.signal.emit(self.generate_params())

    def last_page_handler(self):
        self.current_page = self.number_of_pages
        self.signal.emit(self.generate_params())

    def first_page_handler(self):
        self.current_page = 0
        self.signal.emit(self.generate_params())

    def next_page_handler(self):
        self.current_page += 1
        self.signal.emit(self.generate_params())

    def previous_page_handler(self):
        self.current_page -= 1
        self.signal.emit(self.generate_params())

    def refresh_signal(self):
        self.signal.emit(self.generate_params())