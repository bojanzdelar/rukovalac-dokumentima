from typing import List

from PySide6.QtCore import Signal, QModelIndex
from PySide6.QtGui import QBrush, QColor
from PySide6.QtWidgets import QTableWidget, QAbstractItemView, QTableWidgetItem, QHeaderView

from authentication.model.user import User


class UserTableWidget(QTableWidget):
    signal = Signal(User)

    def __init__(self, parent, users: List[User]):
        self.users = users
        super().__init__(parent)
        self.setColumnCount(4)
        self.setHorizontalHeaderLabels(['Id', 'Number', 'Email', 'Role'])
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QAbstractItemView.SelectionBehavior.SelectRows)
        self.setSelectionMode(QAbstractItemView.SingleSelection)

        self._populate_cells(users)
        # self.setStyleSheet('background-color: red;')
        self.doubleClicked.connect(self.row_double_clicked)

    def rerender(self, users: List[User]):
        self._populate_cells(users)

    def row_double_clicked(self, index: QModelIndex):
        index = index.row()
        self.signal.emit(self.users[index])

    def _populate_cells(self, users: List[User]):
        self.users = users
        amount_of_users = len(self.users)
        self.setRowCount(amount_of_users)
        for index, usr in enumerate(users):
            item = QTableWidgetItem(usr.id)
            item1 = QTableWidgetItem(usr.user_number)
            item2 = QTableWidgetItem(usr.username)
            item3 = QTableWidgetItem(usr.permissions[0])
            if not usr.enabled:
                item.setForeground(QBrush(QColor(255, 0, 0)))
                item1.setForeground(QBrush(QColor(255, 0, 0)))
                item2.setForeground(QBrush(QColor(255, 0, 0)))
                item3.setForeground(QBrush(QColor(255, 0, 0)))
            self.setItem(index, 0, item)
            self.setItem(index, 1, item1)
            self.setItem(index, 2, item2)
            self.setItem(index, 3, item3)
        self.horizontalHeader().setStretchLastSection(True)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)