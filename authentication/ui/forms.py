import uuid

from PySide6.QtGui import Qt
from PySide6.QtWidgets import QPushButton, QWidget, QGridLayout, QLabel, \
    QLineEdit, QComboBox


class LoginForm(QWidget):
    def __init__(self):
        # form
        super().__init__()
        self.layout_login_form = QGridLayout()
        q_label = QLabel("User Number")
        q_label.setMaximumWidth(120)
        q_label.setAlignment(Qt.AlignRight)
        password_ = QLabel("Password")
        password_.setMaximumWidth(120)
        password_.setAlignment(Qt.AlignRight)
        self.layout_login_form.addWidget(q_label, 0, 0)
        self.layout_login_form.addWidget(password_, 1, 0)
        self.setLayout(self.layout_login_form)
        self.layout_login_form.setAlignment(Qt.AlignCenter)

        # # form inputs
        self.password_text = QLineEdit()
        self.password_text.setMaximumWidth(300)
        self.password_text.setEchoMode(QLineEdit.EchoMode.Password)
        self.layout_login_form.addWidget(self.password_text, 1, 1)
        self.username_text = QLineEdit()
        self.username_text.setMaximumWidth(300)
        self.layout_login_form.addWidget(self.username_text, 0, 1)

        # # Button
        self.submit_form_btn = QPushButton("Submit")
        self.layout_login_form.addWidget(self.submit_form_btn, 2, 1, Qt.AlignCenter)

        self.forgot_password_form_btn = QPushButton("Forgot password")
        self.layout_login_form.addWidget(self.forgot_password_form_btn, 3, 1, Qt.AlignCenter)
        # self.submit_form_btn.connect.cli

        # # add form

    def clear_input(self):
        self.password_text.clear()
        self.username_text.clear()


class RegistrationForm(QWidget):
    def __init__(self):

        # form
        super().__init__()
        self.layout_register_form = QGridLayout()
        email_label = QLabel("Username/Email")
        email_label.setMaximumWidth(120)
        email_label.setAlignment(Qt.AlignRight)
        password_label = QLabel("Password")
        password_label.setMaximumWidth(120)
        password_label.setAlignment(Qt.AlignRight)

        role_label = QLabel("Role")
        role_label.setMaximumWidth(120)
        role_label.setAlignment(Qt.AlignRight)

        user_number = QLabel("User number")
        user_number.setMaximumWidth(120)
        user_number.setAlignment(Qt.AlignRight)
        self.layout_register_form.addWidget(email_label, 0, 0)
        self.layout_register_form.addWidget(password_label, 1, 0)
        self.layout_register_form.addWidget(user_number, 2, 0)
        self.layout_register_form.addWidget(role_label, 3, 0)
        self.setLayout(self.layout_register_form)
        self.layout_register_form.setAlignment(Qt.AlignCenter)
        # self.setMaximumWidth(400)

        # # form inputs
        self.password_text_register = QLineEdit()
        self.password_text_register.setMaximumWidth(300)
        self.password_text_register.setEchoMode(QLineEdit.EchoMode.Password)
        # self.password_text_register.setText(uuid.uuid4().__str__()[0:12])
        self.layout_register_form.addWidget(self.password_text_register, 1, 1)

        self.user_number_text = QLineEdit()
        self.user_number_text.setMaximumWidth(300)
        self.layout_register_form.addWidget(self.user_number_text, 2, 1)
        self.username_text_register = QLineEdit()
        self.username_text_register.setMaximumWidth(300)
        self.layout_register_form.addWidget(self.username_text_register, 0, 1)

        # select input
        self.cb = QComboBox()
        self.cb.addItems(["User", "Admin"])
        self.cb.setMaximumWidth(300)
        self.layout_register_form.addWidget(self.cb, 3, 1)
        self.cb.currentIndexChanged.connect(self.selectionchange)

        # # Button
        self.submit_form_register_btn = QPushButton("Submit")
        self.layout_register_form.addWidget(self.submit_form_register_btn, 4, 1, Qt.AlignCenter)
        self.selected_role = "ROLE_USER"

    def selectionchange(self, i):
        selected = self.cb.currentText()
        if selected == "Admin":
            self.selected_role = "ROLE_ADMIN"
        else:
            self.selected_role = "ROLE_USER"

    def clear_input(self):
        self.password_text_register.clear()
        # self.password_text_register.setText(uuid.uuid4().__str__()[0:12])
        self.user_number_text.clear()
        self.username_text_register.clear()
