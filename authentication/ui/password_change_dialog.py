from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, QPushButton, QWidget, QBoxLayout, QMessageBox

from authentication.service.auth_service import AuthService
from authentication.service.auth_service import AuthService


class PasswordResetDialog(QDialog):

    def _cancel_clicked(self):
        self.close()

    def _submit_clicked(self):
        current_password = self.current_password_text.text().strip()
        new_password = self.new_password_text.text().strip()
        confirm_password = self.confirm_password_text.text().strip()
        form_data = {'current_password': current_password, 'new_password': new_password, 'confirm_password': confirm_password}
        [password_change_succ, message] = self.auth_service.change_password_event(form_data)
        if password_change_succ:
            QMessageBox.information(self, "Password reset", message)
            self.close()
        else:
            QMessageBox.critical(self, "Password reset", message)

    def __init__(self, parent=None):
        super(PasswordResetDialog, self).__init__(parent)
        self.auth_service: AuthService = None
        # Auth service
        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service

        # Dialog
        self.setWindowTitle("Reset Password")
        self.setModal(True)
        self.setFixedWidth(400)
        self.setFixedHeight(200)

        self.layout = QGridLayout()

        # Form
        # Labels
        q_label = QLabel("Current Password")
        q_label.setMaximumWidth(120)
        q_label.setAlignment(Qt.AlignRight)
        password_ = QLabel("New password")
        password_.setMaximumWidth(120)
        password_.setAlignment(Qt.AlignRight)
        confirm_password_ = QLabel("Confirm Password")
        confirm_password_.setMaximumWidth(120)
        confirm_password_.setAlignment(Qt.AlignRight)

        self.layout.addWidget(q_label, 0, 0)
        self.layout.addWidget(password_, 1, 0)
        self.layout.addWidget(confirm_password_, 2, 0)
        self.setLayout(self.layout)
        self.layout.setAlignment(Qt.AlignCenter)

        # Inputs

        # # 1
        self.new_password_text = QLineEdit()
        self.new_password_text.setMaximumWidth(300)
        self.new_password_text.setEchoMode(QLineEdit.EchoMode.Password)
        self.layout.addWidget(self.new_password_text, 1, 1)

        # # 2
        self.confirm_password_text = QLineEdit()
        self.confirm_password_text.setMaximumWidth(300)
        self.confirm_password_text.setEchoMode(QLineEdit.EchoMode.Password)
        self.layout.addWidget(self.confirm_password_text, 2, 1)

        # # 3
        self.current_password_text = QLineEdit()
        self.current_password_text.setMaximumWidth(300)
        self.current_password_text.setEchoMode(QLineEdit.EchoMode.Password)
        self.layout.addWidget(self.current_password_text, 0, 1)

        # # Button
        self.btn_container_widget = QWidget(self)
        self.btn_container_layout = QBoxLayout(QBoxLayout.LeftToRight, self.btn_container_widget)
        self.btn_container_widget.setLayout(self.btn_container_layout)
        self.submit_form_change_password_btn = QPushButton("Submit")
        self.cancel_form_change_password = QPushButton("Cancel")
        self.btn_container_layout.addWidget(self.submit_form_change_password_btn)
        self.btn_container_layout.addWidget(self.cancel_form_change_password)
        self.layout.addWidget(self.btn_container_widget, 3, 1, Qt.AlignCenter)
        self.cancel_form_change_password.clicked.connect(self._cancel_clicked)
        self.submit_form_change_password_btn.clicked.connect(self._submit_clicked)


