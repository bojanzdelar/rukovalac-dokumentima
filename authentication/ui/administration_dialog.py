import math
import re
from typing import List

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QMessageBox, QDialog

from authentication.model.application_context import ApplicationContext
from authentication.model.user import User
from authentication.ui.forms import RegistrationForm
from authentication.ui.modify_user_dialog import ModifyUserDialog
from authentication.ui.search_box_widget import SearchBoxWidget
from authentication.ui.user_table_widget import UserTableWidget


class AdministrationDialog(QDialog):
    def __init__(self, admin_service, users: List[User], parent=None):
        super().__init__(parent)
        self.setWindowTitle("Document++ administration")
        self.setFixedSize(1300, 650)

        self.admin_service = admin_service
        # Users
        self.users = users
        # Layout
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        # btn
        self.btn_container_widget = QWidget()
        self.btn_layout = QHBoxLayout()
        self.navigateManageUsersBtn = QPushButton("Manage Users")
        self.navigateRegisterBtn = QPushButton("Register a new User")
        self.navigateRegisterBtn.setMaximumHeight(100)
        self.navigateManageUsersBtn.setMaximumHeight(100)
        self.btn_layout.addWidget(self.navigateRegisterBtn)
        self.btn_layout.addWidget(self.navigateManageUsersBtn)
        self.btn_container_widget.setLayout(self.btn_layout)
        self.btn_layout.setAlignment(Qt.AlignTop)
        self.btn_container_widget.setMaximumHeight(100)
        # self.btn_container_widget.setStyleSheet('background-color: green;')

        # Styles
        self.navigateRegisterBtn.clicked.connect(lambda: self.auth_on_click_navigation_handler("REGISTER"))
        self.navigateManageUsersBtn.clicked.connect(lambda: self.auth_on_click_navigation_handler("MANAGE"))
        self.auth_on_click_navigation_handler("LOGIN")

        # add buttons
        self.layout.addWidget(self.btn_container_widget, 0, Qt.AlignTop)

        # # # # # #
        # SEARCH  #
        # # # # # #
        [params, filtered] = self.generate_params_and_filtered_users(users, "", 0, 20)

        self.searchWidget = SearchBoxWidget(self, params)
        self.searchWidget.signal.connect(self.handle_navigation)

        self.layout.addWidget(self.searchWidget, 1)

        # # # # # # #

        # # # # # #
        # TABLE # #
        # # # # # #
        self.tableWidget = UserTableWidget(self, filtered)
        self.tableWidget.signal.connect(self.handle_double_click)
        self.tableWidget.setFixedHeight(500)
        self.layout.addWidget(self.tableWidget, 2, Qt.AlignTop)
        # # # # # #

        # # # # # #
        # R E G # #
        # # # # # #
        self.registration_widget = RegistrationForm()
        self.layout.addWidget(self.registration_widget, 2, Qt.AlignTop)
        # # # # # #

        self.auth_on_click_navigation_handler("MANAGE")

        self.registration_widget.submit_form_register_btn.clicked.connect(self.registration_btn_clicked_handler)

    def handle_double_click(self, user: User):
        dialog = ModifyUserDialog(self, user, self.searchWidget.refresh_signal)
        dialog.show()

    def handle_navigation(self, params: dict):
        [params, filtered] = self.generate_params_and_filtered_users(self.admin_service.auth_service.users, params["search_string"], params['current_page'], params["items_per_page"])
        self.searchWidget.refresh_info(params)
        self.tableWidget.rerender(filtered)

    def generate_params_and_filtered_users(self, users: List[User], filter_word: str, page_target: int, user_per_page_limit: int) -> [dict, List[User]]:
        current_active_user_id = ApplicationContext.get_user_data().id
        self.users = []
        for usr in users:
            if current_active_user_id != usr.id and (re.search(filter_word, usr.username, re.IGNORECASE) or re.search(filter_word, usr.user_number, re.IGNORECASE)):
                self.users.append(usr)

        total_pages = 0
        amount_of_users = len(self.users)
        to_get = user_per_page_limit
        target_ = user_per_page_limit * page_target
        if page_target == 0:
            target_ = 0
        # if target_ >= amount_of_users:
        #     to_get = amount_of_users
        if amount_of_users > 0:
            total_pages = int(math.ceil(amount_of_users/user_per_page_limit))
            total_pages -= 1
            # reduce = to_get == user_per_page_limit
            # if reduce == 0:
            #     total_pages -= 1
        else:
            params = {
                "current_page": page_target,
                "number_of_pages": total_pages,
                "search_string": filter_word

            }
            return [params, []]
        filtered = []
        if target_ == len(self.users):
            filtered.append(self.users[target_-1])
        else:
            filtered = self.users[target_:target_+to_get]


        params = {
            "current_page": page_target,
            "number_of_pages": total_pages,
            "search_string": filter_word

        }

        return [params, filtered]

    def registration_btn_clicked_handler(self):
        username = self.registration_widget.username_text_register.text().strip()
        password = self.registration_widget.password_text_register.text().strip()
        user_number = self.registration_widget.user_number_text.text().strip()
        user_roles = [self.registration_widget.selected_role]

        form_data = {'password': password, 'username': username, 'user_number': user_number, "roles": user_roles}

        [reg_succ, message, user] = self.admin_service.auth_service.register_user_event(form_data)

        if not reg_succ:
            QMessageBox.critical(self, "Registration result", message)
        else:
            QMessageBox.information(self, "Registration result", message)
            self.registration_widget.clear_input()
        self.searchWidget.refresh_signal()

    def auth_on_click_navigation_handler(self, type: str):
        if type == "MANAGE":
            self.navigateRegisterBtn.setDisabled(False)
            self.navigateManageUsersBtn.setDisabled(True)
            self.registration_widget.hide()
            self.tableWidget.show()
            self.searchWidget.show()
        elif type == "REGISTER":
            self.searchWidget.hide()
            self.navigateManageUsersBtn.setDisabled(False)
            self.navigateRegisterBtn.setDisabled(True)
            self.tableWidget.hide()
            self.registration_widget.show()