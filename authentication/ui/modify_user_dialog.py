from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, QCheckBox, QWidget, QComboBox, QPushButton, \
    QBoxLayout, QMessageBox

from authentication.model.user import User
from authentication.service.auth_service import AuthService


class ModifyUserDialog(QDialog):
    def __init__(self, parent, user: User, method):
        super().__init__(parent)
        self.method = method
        self.user = user
        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service

        # Dialog
        self.setWindowTitle("Modifying " + user.username)
        self.setModal(True)
        self.setFixedWidth(400)
        self.setFixedHeight(200)

        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.layout.setHorizontalSpacing(0)
        self.layout.setVerticalSpacing(0)
        self.layout.setSpacing(0)
        self.setContentsMargins(0, 0, 0, 0)

        # Row 1
        password_label = QLabel("Password")
        password_label.setAlignment(Qt.AlignRight)
        self.password_text = QLineEdit()
        self.password_text.setMaximumWidth(300)
        self.password_text.setEchoMode(QLineEdit.EchoMode.Password)
        self.password_text.setEnabled(False)
        # password_label.setStyleSheet('background-color: purple;')
        # self.password_text.setStyleSheet('background-color: green;')
        self.layout.addWidget(password_label, 0, 0, Qt.AlignTop)
        self.layout.addWidget(self.password_text, 0, 1, Qt.AlignTop)
        # Row 2
        email_label = QLabel("Email")
        email_label.setAlignment(Qt.AlignRight)
        self.layout.addWidget(email_label, 1, 0, Qt.AlignTop)
        self.email_text = QLineEdit()
        self.email_text.setMaximumWidth(300)
        self.layout.addWidget(self.email_text, 1, 1, Qt.AlignTop)
        self.email_text.setText(user.username)
        # Row 3
        number_label = QLabel("Number")
        number_label.setAlignment(Qt.AlignRight)
        self.layout.addWidget(number_label, 2, 0, Qt.AlignTop)
        self.number_text = QLineEdit()
        self.number_text.setMaximumWidth(300)
        self.layout.addWidget(self.number_text, 2, 1, Qt.AlignTop)
        self.number_text.setText(user.user_number)
        # Row 4
        enabled_label = QLabel("")
        enabled_label.setAlignment(Qt.AlignRight)
        self.layout.addWidget(enabled_label, 3, 0, Qt.AlignTop)
        self.enabled_checkbox = QCheckBox("Enabled")
        if user.enabled:
            self.enabled_checkbox.setCheckState(Qt.Checked)
        else:
            self.enabled_checkbox.setCheckState(Qt.Unchecked)
        self.layout.addWidget(self.enabled_checkbox, 3, 1, Qt.AlignTop)

        # Row 5
        passwd_label = QLabel("")
        passwd_label.setAlignment(Qt.AlignRight)
        self.layout.addWidget(passwd_label, 4, 0, Qt.AlignTop)
        self.should_update_password_checkbox = QCheckBox("Update Password")
        self.should_update_password_checkbox.stateChanged.connect(self.handle_password_checkbox_state_change)
        self.should_update_password_checkbox.setCheckState(Qt.Unchecked)
        self.layout.addWidget(self.should_update_password_checkbox, 4, 1, Qt.AlignTop)
        # Row 6
        self.selected_role = user.permissions[0]
        role_label = QLabel("Role")
        role_label.setAlignment(Qt.AlignRight)
        self.layout.addWidget(role_label, 5, 0, Qt.AlignTop)
        self.cb = QComboBox()
        self.cb.addItems(["User", "Admin"])
        if self.selected_role == "ROLE_USER":
            self.cb.setCurrentIndex(0)
        else:
            self.cb.setCurrentIndex(1)
        self.cb.setMaximumWidth(300)
        self.layout.addWidget(self.cb, 5, 1, Qt.AlignTop)
        self.cb.currentIndexChanged.connect(self.selectionchange)
        # Row 7
        # # Button
        self.btn_container_widget = QWidget(self)
        self.btn_container_layout = QBoxLayout(QBoxLayout.LeftToRight, self.btn_container_widget)
        self.btn_container_widget.setLayout(self.btn_container_layout)
        self.confirm_btn = QPushButton("Confirm")
        self.confirm_btn.clicked.connect(self._submit_clicked)
        self.cancel_btn = QPushButton("Abort")
        self.cancel_btn.clicked.connect(self._cancel_clicked)
        self.btn_container_layout.addWidget(self.cancel_btn)
        self.btn_container_layout.addWidget(self.confirm_btn)
        self.layout.addWidget(self.btn_container_widget, 6, 1, Qt.AlignCenter)

    def handle_password_checkbox_state_change(self):
        if self.should_update_password_checkbox.isChecked():
            self.password_text.setEnabled(True)
        else:
            self.password_text.setEnabled(False)

    def selectionchange(self, i):
        selected = self.cb.currentText()
        if selected == "Admin":
            self.selected_role = "ROLE_ADMIN"
        else:
            self.selected_role = "ROLE_USER"

    def _cancel_clicked(self):
        self.close()

    def _submit_clicked(self):
        form_value = {"user_number": self.number_text.text(), "username": self.email_text.text(), "roles": [self.selected_role], "enabled": self.enabled_checkbox.isChecked(), "should_change_password": self.should_update_password_checkbox.isChecked()}
        if self.should_update_password_checkbox.isChecked():
            form_value["password"] = self.password_text.text()

        [succ, message] = self.auth_service.admin_modify_user(form_value, self.user)

        if succ:
            self.method()
            QMessageBox.information(self, "User modification", message)
            self.close()
        else:
            QMessageBox.critical(self, "User modification", message)
