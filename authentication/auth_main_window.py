from PySide6.QtGui import QIcon, Qt, QPixmap
from PySide6.QtWidgets import QMainWindow, QVBoxLayout, QWidget, QLabel, \
     QMessageBox

from authentication.model.application_context import ApplicationContext
from authentication.service.auth_service import AuthService
from authentication.ui.forms import  LoginForm


class AuthMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)
        self.setFixedSize(800, 600)
        self.setWindowTitle("Document++ Authentication")
        self.setWindowIcon(QIcon("core/resources/images/app-logo.png"))
        self.layout = QVBoxLayout()
        self.widget.setLayout(self.layout)

        # image
        pixmap = QPixmap("core/resources/images/app-logo.png")
        pixmap.scaled(400, 400, Qt.KeepAspectRatio)
        lbl = QLabel(self)
        lbl.setPixmap(pixmap)
        lbl.setMaximumWidth(400)
        lbl.setMaximumHeight(400)
        lbl.setScaledContents(True)
        self.layout.addWidget(lbl, 0, Qt.AlignCenter)

        # Forms
        # # add form
        self.login_widget = LoginForm()
        self.layout.addWidget(self.login_widget, 10, Qt.AlignBottom)

        self.login_widget.submit_form_btn.clicked.connect(self.login_btn_clicked_handler)

        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service
        self.login_widget.forgot_password_form_btn.clicked.connect(self.forgot_password_handler)

    def forgot_password_handler(self):
        email = self.login_widget.username_text.text()
        [email_sent, message] = self.auth_service.forgot_password_event(email)
        if email_sent:
            QMessageBox.information(self, "Password reset", message)
        else:
            QMessageBox.critical(self, "Password reset", message)

    def login_btn_clicked_handler(self):
        username = self.login_widget.username_text.text().strip()
        password = self.login_widget.password_text.text().strip()

        form_data = {'password': password, 'username': username}

        [log_succ, message, user] = self.auth_service.login_user_event(form_data)

        if not log_succ:
            QMessageBox.critical(self, "Login result", message)
        else:
            # QMessageBox.information(self, "Login result", message)
            self.login_widget.clear_input()
            # login the user
            ApplicationContext(user)

