# Document++

Document++ is a software tool that enables users to create documents that consist of individual pages, which may contain various elements provided by plugins. The tool has been built using Python and Qt library.

The application is following microkernel architecture and component-based development.
Each component is realized using MVC (Model-View-Controller) architectural pattern.

University project for Software Development Methodologies course

**Student**: Filip Zobić - 2019270036

**Student**: Drago Mazić - 2019270138

**Student**: Bojan Zdelar - 2019270983

## Starting the project

Clone the repository: ```git clone https://gitlab.com/bojanzdelar/document-plus-plus```

Create and activate virtual environment: ```python3 -m venv dpp && . dpp/bin/activate```

Download dependencies: ```pip install -r requirements.txt```

Run ```main.py```

## Modifying the project

After downloading new library run this command ```pip3 freeze > requirements.txt```
