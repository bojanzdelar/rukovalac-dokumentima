from .interface.plugin_manager_in import PluginManagerIn
from .model.plugin_registry import PluginRegistry
from .ui.plugin_dialog import PluginDialog

class PluginManager(PluginManagerIn):
    def __init__(self, iface):
        self.iface = iface
        self.plugin_registry = PluginRegistry("plugins", iface, [])

    def show_dialog(self, parent=None):
        PluginDialog(self.plugin_registry, parent).exec()