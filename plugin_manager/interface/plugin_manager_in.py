from abc import ABC, abstractmethod

class PluginManagerIn(ABC):
    @abstractmethod
    def show_dialog(self, parent):
        ...