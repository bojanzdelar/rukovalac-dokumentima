import os
import inspect
import importlib
import json
from PySide6.QtWidgets import QMessageBox
from authentication.model.application_context import ApplicationContext
from authentication.service.auth_service import AuthService
from .plugin import Plugin
from .plugin_specification import PluginSpecification

class PluginRegistry:
    def __init__(self, path, iface, plugins=None):
        if plugins is None:
            plugins = []
        self.path = path # folder
        self.iface = iface # interface (main_window)
        self._plugins = plugins
        self.init_plugins()

        # Login start
        # document-editor zavisi od page-editor
        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service

        user = ApplicationContext.get_user_data()
        for plugin in self._plugins:
            plugin_id = plugin.plugin_specification.id
            user_plugin_setting = user.plugins
            for key, value in user_plugin_setting.items():
                if plugin_id == key:
                    if value:
                        for d in plugin.plugin_specification.dependencies:
                            for p in self._plugins:
                                if p.plugin_specification.id == d.id:
                                    p.is_active = True
                                    break

                        self.activate(plugin_id)
                        break
        self.auth_service.replace_user(user)


    def install(self, plugin):
        exsists = self._check_existing_plugin(plugin.plugin_specification.id)
        if not exsists:
            self._plugins.append(plugin)

    def uninstall(self, plugin):
        self.deactivate(plugin.plugin_specification.id)
        self._plugins.remove(plugin)

    def activate(self, _id, parent_list_id=None):
        if parent_list_id == None:
            parent_list_id = [_id]
            
        plugin = self.get_plugin(_id)
        if not plugin:
            return

        # dependencies check
        missing_plugins_msg = f"In order for {plugin.plugin_specification.name} to be turned on, these plugins must be turned on as well:"
        msg = []
        not_found_msg = f"The following plugins are not installed and are required for {plugin.plugin_specification.name} to work:"
        not_found = False
        for i in plugin.plugin_specification.dependencies:
            msg.append(["#"])
            plugin_found = False
            for p in self._plugins:
                if i.id == p.plugin_specification.id:
                    if i.version <= p.plugin_specification.version: # I go with the assumption that newer versions of the plugin support older versions
                        plugin_found = True
                        if not p.is_active and not p.plugin_specification.id in parent_list_id: # search for the next version if the desired is not installed 
                            if p.plugin_specification.version <= msg[-1][0] or msg[-1][0] == "#":
                                parent_list_id.append(p.plugin_specification.id)
                                msg[-1] = [
                                    p.plugin_specification.id,
                                    "name: {}, version: {}".format(p.plugin_specification.name, p.plugin_specification.version)
                                ]
                        else: # if 1 version is active which is satisfactory
                            msg.pop(-1)
                            break

            if not plugin_found:
                not_found_msg += "\nID: {}, version: {}".format(i.id, i.version)
                not_found = True

        if not_found:
            msg_box = QMessageBox(text=not_found_msg)
            msg_box.setWindowTitle("Plugin manager")
            msg_box.setStandardButtons(QMessageBox.Ok)
            yes_no = msg_box.exec()
        else:
            if len(msg) == 0:
                plugin.activate()

            else:
                counter = 1
                for i in msg:
                    missing_plugins_msg += "\n({}) ".format(counter) + i[1]
                    counter += 1

                msg_box = QMessageBox(text=missing_plugins_msg)
                msg_box.setWindowTitle("Plugin manager")
                msg_box.setStandardButtons(QMessageBox.Yes|QMessageBox.No)

                yes_no = msg_box.exec()
                if yes_no == QMessageBox.Yes:
                    for i in msg:
                        self.activate(i[0], parent_list_id)
                    plugin.activate()

    def deactivate(self, _id, parent_list_id=None):
        if parent_list_id == None:
            parent_list_id = [_id]

        plugin = self.get_plugin(_id)
        if not plugin:
            return

        # checks if some plugin is dependent on the selected plugin, and if so, it is deactivated
        deactive_msg = f"Do you really want to turn off {plugin.plugin_specification.name}, because the plugins that use it will be turned off as well:"
        counter = 1
        msg = []
        for p in self._plugins:
            for i in p.plugin_specification.dependencies:
                if i.id == plugin.plugin_specification.id:
                    if i.version <= plugin.plugin_specification.version:
                        if p.is_active and not p.plugin_specification.id in parent_list_id:
                            parent_list_id.append(p.plugin_specification.id)
                            msg.append([
                                p.plugin_specification.id,
                                "\n({}) name: {}, version: {}".format(counter, p.plugin_specification.name, p.plugin_specification.version)
                            ])
                            counter += 1
                            break

        if len(msg) == 0:
            plugin.deactivate()
        else:
            for i in msg:
                deactive_msg += i[1]

            msg_box = QMessageBox(text=deactive_msg)
            msg_box.setWindowTitle("Plugin manager")
            msg_box.setStandardButtons(QMessageBox.Yes|QMessageBox.No)

            yes_no = msg_box.exec()
            if yes_no == QMessageBox.Yes:
                for i in msg:
                    self.deactivate(i[0], parent_list_id)
                plugin.deactivate()

    def init_plugins(self):
        """
        Loads all plugins from path.
        """
        plugins_packages = os.listdir(self.path)
        for package in plugins_packages:
            package_path = os.path.join(self.path, package)
            plugin_path = os.path.join(package_path, "plugin.py") # Po dogovoru glavni deo plugin-a cuvamo u plugin.py
            spec_path = os.path.join(package_path, "specification.json") # specifikacija svakog plugina ce se nalaziti
            # u ovoj dateoteci

            data = {}
            with open(spec_path) as fp:
                data = json.load(fp)
            specification = PluginSpecification.from_dict(data) # TODO load from here
            # dynamic module loading
            plugin = importlib.import_module(plugin_path.replace(os.path.sep, ".").rstrip(".py"))
            clsmembers = inspect.getmembers(plugin_path, inspect.isclass)
            if len(clsmembers) == 1:
                plugin = plugin.Plugin(specification, self.iface) # unutar modula ce postojati tacno jedna klasa koju cemo
                # zvati Plugin
                # plugin installation
                self.install(plugin)
            else:
                raise IndexError("The plugin.py module must contain just one class!")

    def get_plugin(self, plugin_id: int) -> Plugin:
        for plugin in self._plugins:
            if plugin_id == plugin.plugin_specification.id:
                return plugin

    def _check_existing_plugin(self, _id):
        """
        Checks if plugin with _id is already in plugin list.
        """
        for plugin in self._plugins:
            if plugin.plugin_specification.id == _id:
                return True
        return False

    def get_all_plugins_status(self):
        lista = []
        for plugin in self._plugins:
            lista.append([
                plugin.is_active,
                plugin.plugin_specification.name,
                plugin.plugin_specification.version,
                plugin.plugin_specification.id,
                plugin.plugin_specification.dependencies
            ])

        return lista

    def change_is_active(self, plugins, checked):
        for i in range(len(plugins)):
            for p in self._plugins:
                if plugins[i][3] == p.plugin_specification.id:
                    p.is_active = checked[i]
                    break

    def update_status_plugins(self, plugins, checked):
        self.change_is_active(plugins, checked)

        for i in range(len(plugins)-1, -1, -1):
            if not plugins[i][0] and checked[i]:
                ApplicationContext.get_user_data().plugins[plugins[i][3]] = True
                self.activate(plugins[i][3])
            elif plugins[i][0] and not checked[i]:
                ApplicationContext.get_user_data().plugins[plugins[i][3]] = False
                self.deactivate(plugins[i][3])