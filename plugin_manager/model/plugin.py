from abc import ABC, abstractmethod

class Plugin(ABC):

    @abstractmethod
    def activate(self):
        ...

    @abstractmethod
    def deactivate(self):
        ...
    