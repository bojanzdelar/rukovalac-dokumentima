from PySide6 import QtWidgets
from PySide6.QtGui import QIcon

from authentication.model.application_context import ApplicationContext
from authentication.service.auth_service import AuthService
from .plugin_table import PluginTable

class PluginDialog(QtWidgets.QDialog):
    def __init__(self, plugin_registry, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Plugins Management")

        auth_service = AuthService.get_instance()
        if auth_service is None:
            auth_service = AuthService()
        self.auth_service = auth_service

        self.setWindowIcon(QIcon("core/resources/images/app-logo.png"))
        self.plugin_registry = plugin_registry
        self.main_layout = QtWidgets.QVBoxLayout()
        self.resize(600, 500)
        self.central_widget = PluginTable(self.plugin_registry)
        self.main_layout.addWidget(self.central_widget)

        self.btn_confirm = QtWidgets.QPushButton(text="Confirm")
        self.btn_confirm.clicked.connect(self.confirm)

        self.btn_cancel = QtWidgets.QPushButton(text="Cancel")
        self.btn_cancel.clicked.connect(self.close)

        self.main_layout.addWidget(self.btn_confirm)
        self.main_layout.addWidget(self.btn_cancel)

        self.setLayout(self.main_layout)
        self.show()
        
    def confirm(self):
        checked = []
        for i in range(len(self.central_widget.check_boxs)):
            checked.append(self.central_widget.check_boxs[i].is_active)
        self.plugin_registry.update_status_plugins(self.central_widget.plugins, checked)
        self.auth_service.replace_user(ApplicationContext.get_user_data())
        self.close()