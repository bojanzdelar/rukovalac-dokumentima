from PySide6 import QtWidgets
from PySide6.QtGui import QStandardItem, QStandardItemModel
from PySide6.QtWidgets import QAbstractItemView, QHeaderView, QTableView, QWidget
from PySide6.QtCore import Qt
from .align_delegate import AlignDelegate

class PluginTable(QWidget):
    def __init__(self, plugin_registry, parent=None):
        super(PluginTable, self).__init__(parent)
        self.plugin_registry = plugin_registry
        self.pocetna_strana = parent
        self.main_layout = QtWidgets.QVBoxLayout()
        self.btn_layout = QtWidgets.QHBoxLayout()
        self.table = QtWidgets.QTableView(self)
        self.table.setUpdatesEnabled(True)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.read()
        self.main_layout.addLayout(self.btn_layout)
        self.main_layout.addWidget(self.table)
        self.table.keyPressEvent = self.keyPressEvent
        self.setLayout(self.main_layout)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Down or event.key() == Qt.Key_Up:
            event.ignore()
            return
        else:
            super(QTableView, self.table).keyPressEvent(event)

    def read(self):
        model = self.show_plugins()
        self.table.setModel(model)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        header = self.table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        header.setDefaultAlignment(Qt.AlignCenter)
        delegate = AlignDelegate(self.table)
        self.table.setItemDelegate(delegate)


    def show_plugins(self):
        column_names = ['Name', 'Version']
        tableModel = QStandardItemModel();

        for i in range(len(column_names)):
            header = QStandardItem(column_names[i])
            tableModel.setHorizontalHeaderItem(i, header)
            
        self.plugins = self.plugin_registry.get_all_plugins_status()
        self.check_boxs = []
        counter = 0
        for plugin in self.plugins:
            check_box = QStandardItem(plugin[1])
            setattr(check_box, 'is_active', False)
            check_box.setCheckable(True)
            if plugin[0]:
                check_box.setCheckState(Qt.Checked)
                check_box.is_active = True
            else:
                check_box.setCheckState(Qt.Unchecked)
                check_box.is_active = False
                
            tableModel.setItem(counter, 0, check_box);
            tableModel.setItem(counter, 1, QStandardItem(plugin[2]));
            self.check_boxs.append(check_box)
            counter += 1

        tableModel.itemChanged.connect(on_item_changed)
                
        return tableModel
        
 
def on_item_changed(item):
    if item.isCheckable():
        if item.is_active:
            item.is_active = False
        else:
            item.is_active = True
