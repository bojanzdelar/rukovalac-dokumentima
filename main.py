import sys
from PySide6 import QtWidgets

from authentication.model.application_context import ApplicationContextSignalManager
from core.service.window_manager_service import WindowManagerService

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    manager = WindowManagerService(app)

    signal_manager = ApplicationContextSignalManager.get_instance()
    signal_manager.context_change_emitter.connect(manager.on_application_context_change)

    app.exec()