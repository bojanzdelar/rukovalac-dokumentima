from typing import List
import os
from PySide6 import QtWidgets
from PySide6.QtCore import Signal, QModelIndex, QEvent
from PySide6.QtWidgets import QDockWidget, QTreeView, QInputDialog, QMessageBox, QVBoxLayout, QWidget

from authentication.model.application_context import ApplicationContext
from core.service.file_system_service import FileSystemService
from core.service.undo_redo_service import Command
from core.ui.components.tool_bar import Toolbar
from core.util.util_functions import remove_trailing_whitespace, get_file_extension, get_file_path_without_extension
from ..interface.workspace_explorer_in import WorkspaceExplorerIn
from ..model.file_system_model import FileSystemModel
from .create_document_dialog import CreateDocumentDialog

class WorkspaceMeta(type(QDockWidget), type(WorkspaceExplorerIn)):
    ...

class WorkspaceWidget(QDockWidget, WorkspaceExplorerIn, metaclass=WorkspaceMeta):
    file_clicked = Signal(str)

    def __init__(self, iface, parent):
        super().__init__("Workspace Explorer", parent)
        
        self.iface = iface

        self.setTitleBarWidget(QWidget())
        self.setFeatures(QtWidgets.QDockWidget.NoDockWidgetFeatures)

        self._init_service()
        self._init_tool_bar()
        self._init_tree()  
        self._init_layout()

    def _init_service(self):
        file_system_service = FileSystemService.get_instance()
        if file_system_service is None:
            file_system_service = FileSystemService()
        self.service: FileSystemService = file_system_service
        self.service.create_workspace()

    def _init_tool_bar(self):
        folder_path = "plugins/workspace_explorer/resources/icons/"

        self.tool_bar = Toolbar(self)
        self.tool_bar.add_action("new_file",  folder_path + "document-icon.png", self.new_file, "", "New document")
        self.tool_bar.add_action("new_folder", folder_path + "collection-icon.png", self.create_folder, "", "New collection")
        self.tool_bar.add_action("update_file", folder_path +"rename-icon.png", self.update_file, "", "Rename document/collection")
        self.tool_bar.add_action("delete", folder_path +"delete-icon.png", self.delete_file, "", "Delete document/collection")

    def _init_tree(self):
        self.file_system_model = FileSystemModel(self.service.get_root_directory())
        user = ApplicationContext.get_instance().user
        [can_load, path] = self.service.get_workspace(user.last_working_dir)
        self.file_system_model.setRootPath(path)

        self.tree_view = QTreeView(self)
        self.tree_view.setModel(self.file_system_model)
        self.tree_view.setRootIndex(self.file_system_model.index(path))
        self.tree_view.doubleClicked.connect(self._file_clicked)
        self.tree_view.viewport().installEventFilter(self)
        self.file_system_model.directoryLoaded.connect(lambda: self.tree_view.expandAll())

        if not can_load:
            user.last_working_dir = path
            ApplicationContext.update_user(user)

        for i in range(1, self.file_system_model.columnCount()):
            self.tree_view.hideColumn(i)

    def _init_layout(self):
        layout = QVBoxLayout()
        layout.addWidget(self.tool_bar)
        layout.addWidget(self.tree_view)
        widget = QWidget()
        widget.setLayout(layout)
        self.setWidget(widget)

    def _file_clicked(self, index: QModelIndex):
        path = self.file_system_model.filePath(index)
        if os.path.isfile(path):
            self.iface.open_document(path)

    def update_workspace_label(self):
        self.tree_view.setHeader

    def new_file(self):
        types = self.iface.get_supported_types()
        if not types:
            QMessageBox.warning(self, "Error", "You can't create document, because no document types are supported. Try installing/enabling plugins.")
            return

        dialog = CreateDocumentDialog(types)
        if not dialog.exec():
            return

        name, type = dialog.getInputs()
        new_file_name = f"{name}.{type}"
        [folder_name, folder_path, root] = self._get_selected_file_name_and_path()

        self.service.create_new_file(new_file_name, root, folder_path, folder_name, 'file')
        path = root + "/" + new_file_name if len(folder_path) == 0 else folder_path + "/" + new_file_name
        self.iface.push_command(
            Command(
                self.service.create_new_file,
                self.service.delete_file,
                (new_file_name, root, folder_path, folder_name, 'file',),
                (path,)
                )
            )

    def delete_file(self):
        [file_name, file_path, _] = self._get_selected_file_name_and_path()
        if file_name is not None:
            reply = QMessageBox.question(self, 'Delete the document', f"Are you sure you want to delete {file_name}?", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.service.delete_file(file_path)
                type_file = 'file' if file_path[file_path.rindex('/') : ].find('.') != -1 else 'folder'
                new_path = file_path[0:file_path.rindex('/')] if _ != file_path else None
                self.iface.push_command(
                    Command(
                        self.service.delete_file,
                        self.service.create_new_file,
                        (file_path,),
                        (file_name, _, new_path, file_name, type_file,)
                        )
                    )

    def update_file(self):
        [file_name, file_path, _] = self._get_selected_file_name_and_path()

        if not file_name:
            return

        is_directory = self.service.check_if_path_is_directory(file_path)
        res = "collection" if is_directory else "document"
        old_name = file_name if is_directory else get_file_path_without_extension(file_name)

        updated_file_name, ok = QInputDialog.getText(self, f"Rename the {res}", f"New {res} name:", text=old_name)
        updated_file_name = remove_trailing_whitespace(updated_file_name)

        if not ok or not updated_file_name:
            return

        if not is_directory:
            updated_file_name += f".{get_file_extension(file_name)}"

        self.service.update_file(updated_file_name, file_name, file_path)
        self.iface.push_command(
            Command(
                self.service.update_file,
                self.service.update_file,
                (updated_file_name, file_name, file_path,),
                (file_name, updated_file_name, file_path[ : file_path.rindex('/')+1] + updated_file_name,)
                )
            )

    def create_folder(self):
        [folder_name, folder_path, root] = self._get_selected_file_name_and_path()
        new_folder_name, ok = QInputDialog.getText(self, "Creating a new collection", "Collection name:")
        new_folder_name = remove_trailing_whitespace(new_folder_name)
        if ok and new_folder_name is not None:
            self.service.create_new_file(new_folder_name, root, folder_path, folder_name, 'folder')
            path = root + "/" + new_folder_name if len(folder_path) == 0 else folder_path + "/" + new_folder_name
            self.iface.push_command(
                Command(
                    self.service.create_new_file,
                    self.service.delete_file,
                    (new_folder_name, root, folder_path, folder_name, 'folder',),
                    (path,)
                    )
                )

    def change_workspace(self, path: str):
        succ = self.service.change_dock_workspace(path, self)
        if succ:
            usr = ApplicationContext.get_instance().user
            usr.last_working_dir = path
            ApplicationContext.update_user(usr)

    def change_to_default_workspace(self):
        self.service.change_dock_workspace(self.service.get_root_directory(), self)
        usr = ApplicationContext.get_instance().user
        usr.last_working_dir = self.service.get_root_directory()
        ApplicationContext.update_user(usr)

    def change_supported_types(self, types: List) -> None:
        self.file_system_model.filter_changed(types)

    def _get_selected_file_name_and_path(self) -> [str, str, str]:
        index = self.tree_view.currentIndex()

        if index is None:
            return [None, None]

        file_name = self.tree_view.model().data(index.siblingAtColumn(0))
        file_path = self.file_system_model.filePath(index)
        root = self.file_system_model.rootPath()

        return [file_name, file_path, root]

    def eventFilter(self, watched, event):
        if (watched is self.tree_view.viewport()) and (event.type() == QEvent.MouseButtonPress) \
                and not self.tree_view.indexAt(event.pos()).isValid():
            self.tree_view.selectionModel().clear()
        return super().eventFilter(watched, event)