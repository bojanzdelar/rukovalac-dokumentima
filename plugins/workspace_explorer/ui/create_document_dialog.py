from PySide6.QtWidgets import QComboBox, QDialog, QLineEdit, QDialogButtonBox, QFormLayout, QMessageBox
from core.util.util_functions import remove_trailing_whitespace

class CreateDocumentDialog(QDialog):
    def __init__(self, types, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Create a new document")

        self.name = QLineEdit(self)
        self.type = QComboBox(self)
        self.type.insertItems(0, types)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self);

        layout = QFormLayout(self)
        layout.addRow("Name: ", self.name)
        layout.addRow("Type", self.type)
        layout.addWidget(buttonBox)

        buttonBox.accepted.connect(self._accept)
        buttonBox.rejected.connect(self.reject)

    def _accept(self):
        name = remove_trailing_whitespace(self.name.text())
        if name:
            self.accept()
        else:
            QMessageBox.warning(self, "Error", "Document name can't be empty")

    def getInputs(self):
        return (self.name.text(), self.type.currentText())