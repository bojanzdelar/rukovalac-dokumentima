from PySide6.QtGui import QKeySequence, Qt

from plugin_manager.model.extension import Extension
from .ui.workspace_widget import WorkspaceWidget


class Plugin(Extension):

    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        Extension.__init__(self, specification, iface)
        self.iface = iface
        self._actions = [
            ["file_menu", "Open new workspace", QKeySequence(Qt.CTRL + Qt.Key_W)],
            ["file_menu", "Default workspace", QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_R)]
        ]
        self.is_active = False

    def activate(self):
        self.is_active = True
        self.iface.add_workspace_explorer(WorkspaceWidget(self.iface, self.iface.mw), self._actions)

    def deactivate(self):
        self.is_active = False
        self.iface.remove_workspace_explorer(self._actions)