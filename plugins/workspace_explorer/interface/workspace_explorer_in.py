from abc import ABC, abstractmethod
from typing import List

class WorkspaceExplorerIn(ABC):
    @abstractmethod
    def change_workspace(self, path: str) -> None:
        ...

    @abstractmethod
    def change_to_default_workspace(self) -> None:
        ...

    @abstractmethod
    def change_supported_types(self, types: List) -> None:
        ...