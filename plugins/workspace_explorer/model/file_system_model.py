from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QFileSystemModel
from core.util.util_functions import get_file_name

class FileSystemModel(QFileSystemModel):
    def __init__(self, root, parent=None):
        super().__init__(parent)

        self.setRootPath(root)
        self.filters = []

        self.setNameFilterDisables(False)
        self._set_filter()

    def headerData(self, section, orientation, role):
        if section == 0 and role == Qt.DisplayRole:
            return f"Workspace: {get_file_name(self.rootPath())}"
        return super().headerData(section, orientation, role=role)

    def data(self, index, role=Qt.DisplayRole):
        if index.column() == 0 and role == Qt.DecorationRole:
            file = self.fileInfo(index)
            if file.isDir():
                return QIcon("plugins/workspace_explorer/resources/icons/collection-icon.png")
            return QIcon("plugins/workspace_explorer/resources/icons/document-icon.png")
        return super().data(index, role)

    def filter_changed(self, new_filters: list):
       self.filters = new_filters
       self._set_filter()

    def _set_filter(self):
        if not self.filters:
            filters = ["?"] # if there aren't any filters, hide all files
        else:
            filters = list(map(lambda x: f"*.{x}", self.filters))
        
        self.setNameFilters(filters)