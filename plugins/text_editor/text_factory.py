from plugins.page_editor.element_factory import ElementFactory
from .model.text_handler import TextHandler
from .ui.text_view import TextView


class TextFactory(ElementFactory):
    def create_handler(self, path, parent_path) -> TextHandler:
        return TextHandler(path, parent_path)

    def create_view(self, text, path, parent=None) -> TextView:
        return TextView(text, path, parent)