from PySide6.QtGui import QMouseEvent
from PySide6.QtWidgets import QMessageBox
from plugin_manager.model.extension import Extension
from plugins.page_editor.interface.element_editor_in import ElementEditorIn
from .text_factory import TextFactory


class Plugin(Extension, ElementEditorIn):

    ELEMENT_TYPE = "text"
    ELEMENT_ACTION = "create-text-box"

    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.factory = TextFactory()
        self.is_active = False

    def activate(self):
        self.is_active = True
        self.iface.add_element_editor(self.ELEMENT_TYPE, self)
        self.add_action()

    def deactivate(self):
        self.is_active = False
        self.iface.remove_element_editor(self.ELEMENT_TYPE)
        self.iface.remove_document_action(self.ELEMENT_ACTION)

    def add_action(self):
        if not self.is_active:
            return

        self.iface.add_document_action(self.ELEMENT_ACTION, 
                "plugins/text_editor/resources/icons/create-text-box.png", None, "", "Create text box")

    def insert_element(self, mouse_event: QMouseEvent) -> None:
        if not self.is_active or not self.iface.is_action_checked(self.ELEMENT_ACTION):
            return

        item = self.factory.create_view("New text box", "")
        added = self.iface.request_insert_element(item, mouse_event.x(), mouse_event.y())    

        if added:
            self.iface.set_action_checked(self.ELEMENT_ACTION, False)
        else:
            QMessageBox.warning(self.iface.mw, "Error", "You aren't allowed to add new element on top of existing ones!")