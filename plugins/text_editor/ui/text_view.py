from PySide6.QtCore import Qt
from PySide6.QtWidgets import QGraphicsItem, QGraphicsTextItem
from plugins.page_editor.ui.element_view import ElementView, ElementViewMeta


class TextView(QGraphicsTextItem, ElementView, metaclass=ElementViewMeta):
    data_type = "text"

    def __init__(self, text, path, parent=None):
        super().__init__(text, parent)
        self.path = path
        self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
        self.setTextInteractionFlags(Qt.TextEditorInteraction)

    def mouseMoveEvent(self, event):
        old_x, old_y = self.x(), self.y()

        super().mouseMoveEvent(event)

        self.setPos(*self.new_position(self.x(), self.y(), self.boundingRect(), self.scene()))

        if len(self.collidingItems()):
            self.setPos(old_x, old_y)
    
    def get_data(self):
        return self.toPlainText()
    
    def set_data(self, data):
        self.setPlainText(data)