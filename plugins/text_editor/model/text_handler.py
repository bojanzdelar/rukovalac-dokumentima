from zipfile import ZIP_DEFLATED
from mutablezip import MutableZipFile
from plugins.page_editor.model.element_handler import ElementHandler
from ..ui.text_view import TextView

class TextHandler(ElementHandler):
    def __init__(self, path, parent_path, format="TXT"):
        super().__init__(path, parent_path, format)

    @property
    def asset_prefix(self):
        return "assets/text/text"

    def read_file(self):
        with MutableZipFile(self.parent_path, "r") as z:
            try:
                with z.open(self.path, "r") as file:
                    self.data = file.read().decode("utf-8")
            except FileNotFoundError:
                self.data = ""

    def save_file(self, text_view: TextView):
        with MutableZipFile(self.parent_path, "a", compression=ZIP_DEFLATED) as z:
            z.writestr(self.path, bytes(text_view.toPlainText(), "utf-8"))
