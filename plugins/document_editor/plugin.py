from PySide6.QtGui import QKeyEvent
from plugin_manager.model.extension import Extension
from .interface.document_editor_in import DocumentEditorIn
from .model.document_handler import DocumentHandler
from .ui.document_widget import DocumentWidget
from core.util.util_functions import get_file_name


class Plugin(Extension, DocumentEditorIn):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.is_active = False

    def activate(self):
        self.iface.add_document_editor(self, ["doch"])
        self.is_active = True

    def deactivate(self):
        self.iface.remove_document_editor()
        self.is_active = False
        
    def open_document(self, file_path):
        if self.iface.set_current_document(file_path):
            return

        handler = DocumentHandler(file_path, iface=self.iface)
        widget = DocumentWidget(self.iface.mw.tab_widget, handler, self.iface)

        return get_file_name(file_path), file_path, widget

    def get_current_page(self):
        current_document = self.iface.get_current_document()
        return current_document.page_view

    def scroll_page(self, event: QKeyEvent):
        current_document = self.iface.get_current_document()
        current_document.page_selector.key_press(event)

    def add_element_action(self, name, icon, func, key_press, tooltip):
        all_documents = self.iface.get_all_documents()
        for document in all_documents:
            document.toolbar.add_action(name, icon, func, key_press, tooltip, True, document.toolbar_element_group)

    def remove_element_action(self, name):
        all_documents = self.iface.get_all_documents()
        for document in all_documents:
            document.toolbar.remove_action(name)

    def uncheck_element_actions(self):
        current_document = self.iface.get_current_document()
        if not current_document:
            return

        for action in current_document.toolbar.actions():
            if action.actionGroup() == current_document.toolbar_element_group:
                action.setChecked(False)

    def is_action_checked(self, name) -> bool:
        current_document = self.iface.get_current_document()
        return current_document.toolbar.get_action(name).isChecked()

    def set_action_checked(self, name, checked: bool):
        current_document = self.iface.get_current_document()
        current_document.toolbar.get_action(name).setChecked(checked)

    def update_views(self):
        all_documents = self.iface.get_all_documents()
        for document in all_documents:
            document.update_views()