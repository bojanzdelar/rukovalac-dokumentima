from abc import ABC, abstractmethod
from PySide6.QtGui import QKeyEvent


class DocumentEditorIn(ABC):
    @abstractmethod
    def open_document(self, path) -> tuple:
        ...

    @abstractmethod
    def get_current_page(self):
        ...

    @abstractmethod
    def scroll_page(self, event: QKeyEvent):
        ...

    @abstractmethod
    def add_element_action(self, name, icon, func, key_press, tooltip):
        ...

    @abstractmethod
    def remove_element_action(self, name):
        ...

    @abstractmethod
    def uncheck_element_actions(self):
        ...

    @abstractmethod
    def is_action_checked(self, name) -> bool:
        ...

    @abstractmethod
    def set_action_checked(self, name, checked: bool):
        ...

    @abstractmethod
    def update_views(self):
        ...