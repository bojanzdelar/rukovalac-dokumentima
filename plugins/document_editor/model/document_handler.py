import json
from zipfile import ZIP_DEFLATED, BadZipFile
from mutablezip import MutableZipFile

from core.model.abstract_handler import AbstractHandler


class DocumentHandler(AbstractHandler):
    ARCHIVE_EXTENSION = "zip"

    def __init__(self, path, format="DOCH", iface=None):
        super().__init__(path, format)

        self.data = []
        self.page_handlers = []
        self.iface = iface

    def read_file(self):
        try:
            self._read_existing_file()
        except BadZipFile:
            self._initialize_file()

    def create_new_page_handler(self, path=""):
        handler = self.iface.create_page_handler(path, self.path)
        if path != "":
            handler.read_file()
        else:
            existing_paths = [asset for asset in self.data]
            handler.generate_asset_path(existing_paths)

        return handler

    def insert_item(self, index, handler):
        self.data.insert(index, handler.path)
        self.page_handlers.insert(index, handler)

    def pop_index(self, index):
        self.data.pop(index)
        # TODO: remove this statement
        self.page_handlers.pop(index)

    def _read_existing_file(self):
        with MutableZipFile(self.path, "r") as z:
            with z.open("structure.json", "r") as file:
                try:
                    self.data = json.loads(file.read().decode("utf-8"))
                except json.JSONDecodeError:
                    self.data = []
        self._create_page_handlers()

    
    def _initialize_file(self):
         with MutableZipFile(self.path, "w") as z:
                with z.open("structure.json", "w"):
                    ...

    def _create_page_handlers(self):
        for page_path in self.data:
            handler = self.iface.create_page_handler(page_path, self.path)
            handler.read_file()
            self.page_handlers.append(handler)

    def save_file(self, page_data):
        with MutableZipFile(self.path, "a", compression=ZIP_DEFLATED) as z:
            z.writestr("structure.json", json.dumps(self.data, indent=4).encode("utf-8"))

        existing_paths = []
        for handler in self.page_handlers:
            # physical deletion of removed pages                
            if handler.path not in self.data:
                handler.delete_file()
                self.page_handlers.remove(handler)
                continue

            for asset in handler.data:
                existing_paths.append(asset["path"])

        for handler in self.page_handlers:
            handler.save_file(page_data[handler.path], existing_paths)
