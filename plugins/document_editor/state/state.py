from abc import abstractmethod


class State:
    @abstractmethod
    def changeState(self, main, new_state):...

    def select_page(self, page_selector):
        main = page_selector.parent()
        if main.page_selector.selected != None:
            wgt = main.page_selector.selected
            main.page_selector.select_page(wgt, wgt.selected())