from plugins.document_editor.state.state import State


class EditState(State):
    def changeState(self, main, new_state):
        main.read_mode = new_state
        self.select_page(main)