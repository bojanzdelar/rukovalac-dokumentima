from plugins.document_editor.state.state import State


class ReadState(State):
    def changeState(self, main, new_state):
        main.save_scene()
        main.read_mode = new_state
        self.select_page(main)