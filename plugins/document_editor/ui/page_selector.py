import PySide6
from PySide6.QtCore import QCoreApplication, QSize, Qt
from PySide6.QtGui import QAction, QKeyEvent, QResizeEvent, QWheelEvent
from PySide6.QtWidgets import QListWidget, QListWidgetItem, QMenu, QScrollBar, QWidget
from ..state.read_state import ReadState
from ..state.edit_state import EditState
from .page_preview import PagePreview


class PageSelector(QListWidget):
    def __init__(self, iface, parent=None):
        super().__init__(parent)
        self.iface = iface
        self._init_attributes()
        self.maxw = 300
        self.minw = 200
        self.setMinimumWidth(self.minw)
        self.setMaximumWidth(self.maxw)
        self.page_def_name = "pages/page"
        self.page_def_ext = ".pg"
        self.setDragEnabled(False)
        self.setGridSize(QSize(self.width(), self.width()*0.6))
        self.setVerticalScrollBar(QScrollBar())
        self.horizontalScrollBar().setDisabled(True)
        self.horizontalScrollBar().hide()
        self.setSpacing(5)
        self.selected = None
        self.wgt = None
        self.wgt_index_enter = 0
        self.wgt_index_leave = None
        self.saved = False
        self.counter = 0
        self.scrolling = False
        self.arrow_nav = False
        self.read_mode = False
        self.installEventFilter(self)
    
    def wheelEvent(self, event: QWheelEvent) -> None:
        self.scrolling = True
        numDegrees = event.angleDelta() / 8;
        if not numDegrees.isNull():
            numSteps = numDegrees / 15;
            i = numSteps.y() * -1
            if i == 1 or i == -1:
                self.scroll_select_page(i)

        return super().wheelEvent(event)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        self.key_press(event)

    def key_press(self, event: QKeyEvent) -> None:
        if not self.arrow_nav:  
            self.arrow_nav = True

            if event.key() == 16777235: # int for page_down
                self.scroll_select_page(-1)
            elif event.key() == 16777237: # int for page_up
                self.scroll_select_page(1)
                
            self.arrow_nav = False
    
    def get_all_wgts(self):
        l = []
        for i in range(self.count()):
            l.append(self.get_widget_at(i))
        
        return l

    def read_edit_mode(self, read_mode):
        if not self.read_mode: # Edit > Read
            ReadState().changeState(self, read_mode)
        else: # Read > Edit
            EditState().changeState(self, read_mode)
        
    
    def enterEvent(self, event: PySide6.QtGui.QEnterEvent) -> None:
        self.scrolling = True
        return super().enterEvent(event)
    
    def leaveEvent(self, event: PySide6.QtCore.QEvent) -> None:
        self.scrolling = False
        return super().leaveEvent(event)
    
    def _init_attributes(self):
        self.list_wgt_copy = []
        self.list_name_items = []
        self.copy_wgt = None
        self.cut_wgt = None
    
    def get_current_widget(self):
        return self.get_widget_at(self.selected.getIndex())
    
    def get_item_at(self, row:int):
        return self.findItems(self.list_name_items[row], Qt.MatchExactly)[0]

    def get_widget_at(self, row:int):
        if self.count() <= row:
            return None
        return self.itemWidget(self.get_item_at(row))
    
    def load_document(self):
        self.document = self.parent().handler.page_handlers
        self._init_pages()
    
    def _init_pages(self):
        i = 0
        for handler in self.document:
            self.list_wgt_copy.append([i, handler, self])
            name = self.page_def_name + str(i) + self.page_def_ext
            item = QListWidgetItem(name)
            self.list_name_items.append(name)
            self.addItem(item)
            wgt = PagePreview(*self.list_wgt_copy[-1])
            wgt.setMaximumWidth(self.width())
            item.setSizeHint(wgt.getPage().sizeHint())
            self.setItemWidget(item, wgt)
            i += 1

        if i != 0:
            wgt = self.get_widget_at(0)
            self.select_page(wgt, wgt.selected())
        else:
            self.parent().page_view = QWidget()
            
        self._init_qmenu()
    
    def resizeDock(self):
        w = self.width()
        h = self.height()
        QCoreApplication.postEvent(self, QResizeEvent(QSize(w, h), QSize(w - 1, h - 1)))
        
    def scroll_select_page(self, up_down:int):
        if self.selected:
            index = self.selected.getIndex()
            if index >= 0:
                if index == 0 and up_down < 0:
                    return
                elif index == len(self.list_wgt_copy)-1 and up_down > 0:
                    return
                    
                self.wgt_index_leave = index
                self.wgt_index_enter = index + up_down
                wgt = self.get_widget_at(self.wgt_index_enter)
                wgt.select_page()

    def _switch_widgets(self, start_index:int, end_index:int):
        for i in range(start_index, end_index):
            wgt = self.get_widget_at(i)
            num = self.list_wgt_copy[i][0]
            wgt.setIndex(num)
            wgt.setLabelText(str(num+1))
        
    def _add_item_wgt(self, name:str, index:int, wgt=None):
        if type(wgt) == list:
            wgt = PagePreview(*wgt)
            wgt.setMaximumWidth(self.width())

        wgt.setFixedHeight(self.width()*0.55)
        item = QListWidgetItem(name)
        self.insertItem(index, item)
        self.setItemWidget(item, wgt)
        
        return wgt

    def _update_l_name_and_wgt_copy(self, start_index:int, added:bool):
        num = 1 if added else -1
        index = len(self.page_def_name)
        index1 = len(self.page_def_ext)
        diff = len(self.list_name_items) - start_index
        for i in range(1, diff+1):
            i = -i
            self.list_wgt_copy[i][0] += num
            old_name = self.list_name_items[i]
            text = self.list_name_items[i][0:index] + str(int(self.list_name_items[i][index:-index1]) + num) + self.page_def_ext
            self.list_name_items[i] = text
            item = self.findItems(old_name, Qt.MatchExactly)[0]
            item.setText(text)

    def _update_lists(self, index, wgt=None, name:str=None, added:bool=False, save_scene=True):
        num = 1 if added else 0
        select_wgt = None
        list_not_empty = False
        if not added:
            wgt = self.list_wgt_copy.pop(index)
            self.list_name_items.pop(index)
            self.takeItem(index)
            self._update_l_name_and_wgt_copy(index + num, added)
            self.parent().handler.pop_index(index)
            temp_index = index
            while list_not_empty == False:
                wgt1 = self.get_widget_at(temp_index)
                if wgt1:
                    list_not_empty = True
                    break

                if temp_index != 0:
                    temp_index -= 1
                else:
                    break
            
            if list_not_empty:
                select_wgt = wgt1
                save_scene = False
            self.parent().remove_central_wgt()
        else:
            list_not_empty = True
            if type(wgt) != list:
                wgt_l = [wgt.getIndex(), wgt.handler, self]
            else:
                wgt_l = wgt

            self.list_wgt_copy.insert(index, wgt_l)
            self.list_name_items.insert(index, name)
            self._update_l_name_and_wgt_copy(index + num, added)
            new_wgt = self._add_item_wgt(name, index, wgt)
            self.parent().handler.insert_item(index, wgt_l[1])
            select_wgt = new_wgt

        self._switch_widgets(index + num, len(self.list_wgt_copy))

        if list_not_empty:
            self.select_page(select_wgt, select_wgt.selected(), save_scene)

        self._resize_wgts()
        if not added:
            return wgt

    def add_page(self, after:int=0):
        index = self.wgt_index_enter + after
        if after == 2:
            index = 0

        name = self.page_def_name + str(index) + self.page_def_ext
        handler = self.parent().handler.create_new_page_handler()
        wgt_l = [index, handler, self]
        self._update_lists(index, wgt_l, name, True)

    def _cut_page(self):
        self.save_scene()
        items = self.selected.getPage().scene().items()
        l = [items, True]
        self.cut_wgt = PagePreview(*self._update_lists(self.wgt_index_enter), gen_view_scene=l)
        self.cut_wgt.setMaximumWidth(self.width())
        self.selected = None
    
    def _copy_page(self):            
        self.save_scene()
        wgt = self.get_widget_at(self.wgt_index_enter)
        handler = self.parent().handler.create_new_page_handler()
        items = wgt.getPage().scene().items()
        l = [items, True]
        self.copy_wgt = PagePreview(wgt.getIndex(), handler, self, gen_view_scene=l)
        self.copy_wgt.setMaximumWidth(self.width())
        self.selected = None

    def _paste_check(self):
        if self.copy_wgt:
            wgt = self.copy_wgt
            self.copy_wgt = None

        elif self.cut_wgt:
            wgt = self.cut_wgt
            self.cut_wgt = None
        else:
            return True

        return wgt

    def _paste_page(self, after:int=0):
        wgt = self._paste_check()
        if type(wgt) is bool:
            return

        index = self.wgt_index_enter + after
        wgt.setIndex(index)
        wgt.setLabelText(str(index+1))
        name = self.page_def_name + str(index) + self.page_def_ext
        self._update_lists(index, wgt, name, True)

    def _delete_page(self):
        self._update_lists(self.wgt_index_enter)

    def save_scene(self):
        if self.selected and not self.read_mode:
            items = self.parent().page_view.scene().items()
            old_scene = self.selected.getPage().scene()
            old_items = self.selected.getPage().scene().items()
            item_found = []
            for i in range(len(old_items)):
                item_found.append(i)

            part = []
            
            for item in items:
                box = {}
                box["type"] = item.data_type
                box["path"] = item.path
                box["x"] = abs(item.x())
                box["y"] = abs(item.y())

                found = False
                counter = 0
                for old_item in old_items:
                    if old_item.data_type == item.data_type:
                        if old_item.path == item.path:
                            old_item.setPos(item.x(), item.y())
                            if old_item.get_data() != item.get_data():
                                old_item.set_data(item.get_data())
                            found = True
                            item_found.pop(counter)
                            break
                    counter += 1
                
                if not found:
                    try:
                        view = self.iface.clone_element(item)
                    except KeyError:
                        continue
                    
                    view.setPos(item.x(), item.y())
                    view.path += str(self.counter)
                    self.counter += 1
                    old_scene.addItem(view)

                part.append(box)
              
            for i in item_found:
                old_scene.removeItem(old_items.pop(i))

            self.list_wgt_copy[self.selected.getIndex()][1].data = part

    def select_page(self, wgt, page, save_scene=True):
        page.setInteractive(not self.read_mode)
        if save_scene:
            if self.selected:
                if wgt.getIndex() != self.selected.getIndex():
                    self.save_scene()
        
        self.selected = wgt
        self.parent().page_selected(page)
    
    def save_current_scene(self):
        if self.selected and not self.read_mode:
            self.save_scene()
            wgt = self.selected
            self.selected = None
            self.saved = True
            self.select_page(wgt, wgt.selected(), False)

    def _enable_qaction(self):
        if not self.read_mode:
            if self.wgt == None:
                self.menu.removeAction(self.deletep)
                self.menu.removeAction(self.cut)
                self.menu.removeAction(self.copy)
                self.menu.removeAction(self.paste_after)
                self.menu.removeAction(self.paste_before)
                self.menu.removeAction(self.add_after)
                self.menu.removeAction(self.add_before)
                if len(self.list_wgt_copy) == 0:
                    self.menu.addAction(self.add_first)
                else:
                    self.menu.removeAction(self.add_first)
            elif self.copy_wgt == None and self.cut_wgt == None:
                self.menu.removeAction(self.paste_after)
                self.menu.removeAction(self.paste_before)
                self.menu.removeAction(self.add_first)
                
                self.menu.addAction(self.add_after)
                self.menu.addAction(self.add_before)
                self.menu.addAction(self.cut)
                self.menu.addAction(self.copy)
                self.menu.addAction(self.deletep)
                self.menu.addAction(self.first)
                self.menu.addAction(self.last)
            else:
                self.menu.addAction(self.paste_after)
                self.menu.addAction(self.paste_before)
                
                self.menu.removeAction(self.add_first)
                self.menu.removeAction(self.add_after)
                self.menu.removeAction(self.add_before)
                self.menu.removeAction(self.cut)
                self.menu.removeAction(self.copy)
                self.menu.removeAction(self.deletep)
        elif len(self.list_wgt_copy) >= 1:
            self.menu.removeAction(self.paste_after)
            self.menu.removeAction(self.paste_before)
            self.menu.removeAction(self.add_first)
            self.menu.removeAction(self.add_after)
            self.menu.removeAction(self.add_before)
            self.menu.removeAction(self.cut)
            self.menu.removeAction(self.copy)
            self.menu.removeAction(self.deletep)

            self.menu.addAction(self.first)
            self.menu.addAction(self.last)
        
    def _init_qmenu(self):
        self.menu = QMenu()
        
        self.add_first = QAction("Add first page")
        self.add_after = QAction("Add page after")
        self.add_before = QAction("Add page before")
        self.cut = QAction("Cut page")
        self.copy = QAction("Copy page")
        self.paste_after = QAction("Paste page after")
        self.paste_before = QAction("Paste page before")
        self.deletep = QAction("Delete page")

        self.last = QAction("Go to last page")
        self.first = QAction("Go to first page")
        
    def _action_select_page(self, index):
        wgt = self.get_widget_at(index)
        self.select_page(wgt, wgt.selected())

    def contextMenuEvent(self, event):
        self._enable_qaction()
        action = self.menu.exec(self.mapToGlobal(event.pos()))
        if action and not self.read_mode:
            if action.toolTip() == "Add page after":
                self.add_page(1)
            elif action.toolTip() == "Add page before":
                self.add_page()
            elif action.toolTip() == "Add first page":
                self.add_page(2)
            elif action.toolTip() == "Cut page":
                self._cut_page()
            elif action.toolTip() == "Copy page":
                self._copy_page()
            elif action.toolTip() == "Paste page after":
                self._paste_page(1)
            elif action.toolTip() == "Paste page before":
                self._paste_page()
            elif action.toolTip() == "Delete page":
                self._delete_page()
            elif action.toolTip() == "Go to last page":
                self._action_select_page(-1)
            elif action.toolTip() == "Go to first page":
                self._action_select_page(0)
        elif action:
            if action.toolTip() == "Go to last page":
                self._action_select_page(-1)
            elif action.toolTip() == "Go to first page":
                self._action_select_page(0)
    
    def resizeEvent(self, event: PySide6.QtGui.QResizeEvent) -> None:
        self._resize_wgts()
        return super().resizeEvent(event)

    def _resize_wgts(self):
        w = self.width()
        h = self.height()
        
        self.resize(w, h)
        self.setGridSize(QSize(w, w*0.8))
        for item in self.findItems(self.page_def_name, Qt.MatchStartsWith):
            wgt = self.itemWidget(item)
            item.setSizeHint(wgt.page.sizeHint())
            wgt.resize_wgt()
