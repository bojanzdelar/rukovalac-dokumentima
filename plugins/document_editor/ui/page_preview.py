from PySide6.QtCore import QEvent
from PySide6.QtGui import QKeyEvent, QMouseEvent, Qt
from PySide6.QtWidgets import QLabel, QVBoxLayout, QWidget


class PagePreview(QWidget):
    def __init__(self, index, handler, parent=None, gen_view_scene=None):
        super().__init__(parent)
        self.parent = parent
        self.handler = handler
        self.index = index
        self.space_btw_label_wgt = 5
        self.list_style = {
            "add_border":"border: 1px solid #707070;",
            "select_border":"border: 1px solid #707070;",
            "remove_border":"border: 1px solid #B8B8B8;"
        }
        
        self._init_label()
        self.page = parent.parent().iface.create_page_view(handler, self.parent, gen_view_scene=gen_view_scene)
        self.page.setStyleSheet(self.list_style["remove_border"])
        self._init_layout()
    
    def getPage(self):
        return self.page
    
    def select_page(self, update_parent=True, save_scene=True):
        self._add_border()
        self.parent.wgt_index_enter = self.index
        self.parent.wgt = self
        if not self.parent.saved:
            save_scene = True
        else:
            save_scene = False
            self.parent.saved = False

        if update_parent:
            self.parent.select_page(self, self.selected(), save_scene)
    
    def keyPressEvent(self, event: QKeyEvent) -> None:
        if not self.parent.arrow_nav:
            self.parent.arrow_nav = True

            if event.key() == 16777235: # int for page_down
                self.parent.scroll_select_page(-1)
            elif event.key() == 16777237: # int for page_up
                self.parent.scroll_select_page(1)
                
            self.parent.arrow_nav = False
    
    def mousePressEvent(self, event: QMouseEvent) -> None:
        if not self._is_current_selected():
            self.parent.select_page(self, self.selected())
        return super().mousePressEvent(event)
    
    def _init_layout(self):
        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.addWidget(self.page)
        self.layout.addWidget(self.label)
        self.layout.setSpacing(10)
        self.setLayout(self.layout)
    
    def _init_label(self):
        self.label = QLabel(str(self.index + 1))
        self.label.setStyleSheet("text-align: left;font-size: 10px;font-weight: bold; border: none;")
        self.label.setFixedHeight(10)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setContentsMargins(0,10,0,10)
    
    def setLabelText(self, text):
        self.label.setText(text)
    
    def _add_border(self):
        if self._is_current_selected():
            return
        else:
            if self.parent.selected != None:
                self.parent.selected.page.setStyleSheet(self.list_style["remove_border"])
            self.page.setStyleSheet(self.list_style["select_border"])

    def selected(self):
        self._add_border()
        page = self.parent.parent().iface.create_page_view(self.handler, central_wgt=True, gen_view_scene=self.page.scene().items())
        return page
    
    def resize_wgt(self):
        height_diff_parent = 0.67
        self.setFixedHeight(self.parent.width() * height_diff_parent)
        self.setFixedWidth(self.parent.width())

        new_width = self.width() * 0.95

        space_to_scroll = 0.05
        if new_width < 220:
            space_to_scroll = 0.1
        elif new_width < 270:
            space_to_scroll = 0.075

        if not self.parent.verticalScrollBar().isVisible():
            space_to_scroll = 0
            if new_width < 220:
                space_to_scroll = 0.02

        w_page = new_width - new_width * space_to_scroll

        self.page.setMaximumWidth(w_page)

        self.page.resize_wgt(
            w_page,
            self.parent.width() * height_diff_parent - self.label.height() - self.space_btw_label_wgt
            )

        self.label.setFixedWidth(self.parent.width())
    
    def _is_current_selected(self):
        if self.parent.selected != None:
            if self.parent.selected.getIndex() == self.getIndex():
                return True
                
        return False

    def _enter_state(self):
        if not self._is_current_selected():
            self.page.setStyleSheet(self.list_style["add_border"])
            
        self.parent.wgt_index_enter = self.index
        self.parent.wgt = self

    def enterEvent(self, event) -> None:
        self._enter_state()
        return super().enterEvent(event)
    
    def leaveEvent(self, event: QEvent) -> None:
        if not self._is_current_selected():
            self.page.setStyleSheet(self.list_style["remove_border"])

        self.parent.wgt_index_leave = self.index
        self.parent.wgt = None
        return super().leaveEvent(event)
    
    def setIndex(self, num):
        self.index = num
    
    def getIndex(self):
        return self.index
    