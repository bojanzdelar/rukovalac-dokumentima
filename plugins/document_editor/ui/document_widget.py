from PySide6.QtCore import Qt
from PySide6.QtGui import QActionGroup, QKeyEvent, QWheelEvent
from PySide6.QtWidgets import QGridLayout, QWidget
from core.ui.components.tool_bar import Toolbar
from plugins.page_editor.ui.page_view import PageView
from ..model.document_handler import DocumentHandler
from .page_selector import PageSelector


class DocumentWidget(QWidget):
    def __init__(self, parent, handler: DocumentHandler, iface):
        super().__init__(parent)
        self.iface = iface
        self.icon_path = "plugins/document_editor/resources/icons/"
        self.handler = handler
        self.handler.read_file()

        self.setLayout(QGridLayout(self))
        self._add_toolbar()
        self._add_page_selector()
        self._add_mode_toolbar()

    @property
    def page_view(self):
        try:
            return self.layout().itemAtPosition(1, 0).widget()
        except:
            return None

    @page_view.setter
    def page_view(self, new_page_view):
        if self.page_view:
            old_widget = self.page_view
            self.layout().removeWidget(old_widget)
            old_widget.deleteLater()
            old_widget = None
                    
        self.layout().addWidget(new_page_view, 1, 0, Qt.AlignCenter)
        
    def _add_page_selector(self):
        self.page_selector = PageSelector(self.iface, self)
        self.page_selector.load_document()
        self.installEventFilter(self)
        self.layout().addWidget(self.page_selector, 1, 1)
    
    def _add_toolbar(self):
        self.toolbar = Toolbar(self)
        self.toolbar.add_action("save", self.icon_path + "save.png", self._initate_save, "Ctrl+S", "Save the document")
        self.toolbar.add_action("remove", self.icon_path + "remove.png", self._remove_items, "Del", "Remove selected items")
        self.toolbar.addSeparator()
        self.toolbar_element_group = QActionGroup(self.toolbar)
        self.toolbar_element_group.setExclusionPolicy(QActionGroup.ExclusionPolicy.ExclusiveOptional)
        self.layout().addWidget(self.toolbar, 0, 0, 1, -1)

    def _add_mode_toolbar(self):
        self.mode_toolbar = Toolbar(self)
        self.mode_group = QActionGroup(self.mode_toolbar)
        self.mode_toolbar.add_action("read-mode", self.icon_path + "read-mode.png", self._enable_read_mode, "", "Enable read mode", True, self.mode_group)
        self.mode_toolbar.add_action("edit-mode", self.icon_path + "edit-mode.png", self._enable_edit_mode, "", "Enable edit mode", True, self.mode_group)
        self._enable_read_mode(init=True)
        self.layout().addWidget(self.mode_toolbar, 2, 0, 1, -1)

    def _initate_save(self):
        page_data = {}
        for i in range(self.page_selector.count()):
            widget = self.page_selector.get_widget_at(i)
            page_data[widget.handler.path] = widget.page.scene().items()

        if self.page_view != None and type(self.page_view) != QWidget:
            current_page = self.page_view.scene()
            page_data[current_page.page_handler.path] = current_page.items()
            self.page_selector.save_current_scene()
            self.page_selector.select_page(self.page_selector.selected, self.page_selector.selected.selected(), False)
            
        self.handler.save_file(page_data)  # TODO: move this statement above save_current_scene
                                           # it needs to be executed before save_current_scene to delete unused assets from zip archive

    def get_current_scene(self):
        if type(self.page_view) == PageView:
            return self.page_view.scene()

    def _remove_items(self):
        scene = self.get_current_scene()
        if scene:
            scene.remove_selected_items()

    def _enable_read_mode(self, init=False):
        self.mode_toolbar.get_action("read-mode").setChecked(True)

        if self.page_selector.read_mode:
            return

        if not init:
            size = self.page_view.size()

        self.page_selector.read_edit_mode(True)
        self.toolbar.hide()

        if not init:
            self.page_view.setMinimumSize(size)
        
        if not init:
            self.page_view.setMinimumSize(size)
        
    def _enable_edit_mode(self):
        self.mode_toolbar.get_action("edit-mode").setChecked(True)

        if not self.page_selector.read_mode:
            return

        self.page_selector.read_edit_mode(False)
        self.toolbar.show()

    def update_views(self):
        scene = self.get_current_scene()
        if scene:
            scene.update_items()

        for page_preview_widget in self.page_selector.get_all_wgts():
            page_preview_widget.page.scene().update_items()
    
    def page_selected(self, wgt):
        self.page_view = wgt
        self.page_view.setFocus()

    def remove_central_wgt(self):
        self.page_view = QWidget()
    
    def wheelEvent(self, event: QWheelEvent) -> None:
        if not self.page_selector.scrolling:
            self.page_selector.scrolling = True
            numDegrees = event.angleDelta() / 8;
            if not numDegrees.isNull():
                numSteps = numDegrees / 15;
                i = numSteps.y() * -1
                if i == -1 or i == 1:
                    self.page_selector.scroll_select_page(i)

            self.page_selector.scrolling = False
    
    def keyPressEvent(self, event: QKeyEvent) -> None:
        if not self.page_selector.arrow_nav:
            self.page_selector.arrow_nav = True

            if event.key() == 16777235: # int for page_down
                self.page_selector.scroll_select_page(-1)
            elif event.key() == 16777237: # int for page_up
                self.page_selector.scroll_select_page(1)
                
            self.page_selector.arrow_nav = False
