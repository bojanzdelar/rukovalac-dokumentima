"""
This item is when page tries to display unsupported element
"""

from PySide6.QtGui import QColor
from PySide6.QtWidgets import QGraphicsItem, QGraphicsRectItem, QStyle
from .element_view import ElementView, ElementViewMeta

class SlotView(QGraphicsRectItem, ElementView, metaclass=ElementViewMeta):
    def __init__(self, w, h, path, data_type, parent=None):
        super().__init__(0, 0, w, h, parent)
        self.path = path
        self.data_type = data_type
        self.data = None
        self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
        self.setPen(QColor(0,0,200))

    def mouseMoveEvent(self, event):
        old_x, old_y = self.x(), self.y()

        super().mouseMoveEvent(event)

        self.setPos(*self.new_position(self.x(), self.y(), self.boundingRect(), self.scene()))

        if len(self.collidingItems()):
            self.setPos(old_x, old_y)
    
    def paint(self, painter, option, widget):
        option.state = QStyle.State_None
        return super().paint(painter, option, widget=widget)
    
    def get_data(self):
        return self.data

    def set_data(self, data):
        self.data = data