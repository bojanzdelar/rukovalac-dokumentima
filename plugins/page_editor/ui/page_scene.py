import copy

from PySide6.QtCore import Qt
from PySide6.QtWidgets import QGraphicsItem, QGraphicsScene

from plugins.page_editor.ui.slot_view import SlotView
from plugins.page_editor.ui.scalable import Scalable


class PageScene(QGraphicsScene):
    MAX_ELEMENT_RATIO = 0.4

    def __init__(self, page_handler, parent, iface, gen_view_scene=None):
        super().__init__(parent)
        self.setSceneRect(0, 0, 1200, 800)
        self.page_handler = page_handler
        self.iface = iface
        self.counter = 0
        self.gen_view_scene = gen_view_scene
        if gen_view_scene == None:
            self._generate_view()
        else:
            self._generate_second_view(gen_view_scene)
        
    def _generate_view(self):
        self.document_path = self.page_handler.parent_path

        for e in self.page_handler.data:
            element = self.iface.create_element(e["type"], e["path"], self.document_path)
            if not element:
                element = SlotView(e["width"], e["height"], e["path"], e["type"])
            
            element.setPos(e["x"], e["y"])
            self.addItem(element)
    
    def addItem(self, item: QGraphicsItem) -> None:
        if self.gen_view_scene == None:
            self._read_mode(item)
        return super().addItem(item)
    
    def _read_mode(self, view):
        view.setFlag(QGraphicsItem.ItemIsSelectable, False)
        view.setFlag(QGraphicsItem.ItemIsMovable, False)
        view.setFlag(QGraphicsItem.ItemIsSelectable, False)
        view.setFlag(QGraphicsItem.ItemIsFocusable, False)
    
    def _generate_second_view(self, items):
        self.document_path = self.page_handler.parent_path
        if len(items) == 2:
            if items[1] == True:
                for element in items[0]:
                    element.path = element.path + "_" + str(self.counter)
                    self.counter += 1

                items = items[0]
        
        for element in items:
            view = self.iface.clone_element(element)
            if not view:
                view = SlotView(element.boundingRect().width(), element.boundingRect().height(), element.path, element.data_type)
            view.setPos(element.x(), element.y())
            self.addItem(view)
        
    def _is_occupied(self, x, y):
        return self.itemAt(x, y) is not None

    def add_item(self, item, x, y, scale):
        x /= scale
        y /= scale
        bounding_rect = item.boundingRect()

        if issubclass(type(item), Scalable):
            item.scale_to_page(self.sceneRect().size(), self.MAX_ELEMENT_RATIO)
        
        if self.items(x, y, bounding_rect.width(), bounding_rect.height(), Qt.IntersectsItemShape, Qt.AscendingOrder):
            return False

        item.setPos(x, y)
        self.addItem(item)
        return True

    def update_items(self):
        self.page_handler.data = list(map(self.page_handler.convert_to_json, self.items()))
        self.clear()
        self._generate_view()

    def remove_selected_items(self):
        for item in self.selectedItems():
            self.removeItem(item)

    def mouseMoveEvent(self, event) -> None:
        if len(self.selectedItems()) <= 1:
            return super().mouseMoveEvent(event)