from abc import ABC, abstractmethod

class Scalable(ABC):

    @abstractmethod
    def scale_to_page(self, page_size, max_ratio):
        ...