from PySide6.QtWidgets import QGraphicsView
from PySide6.QtGui import QKeyEvent, QMouseEvent, Qt


class PageView(QGraphicsView):

    SCROLL_UP_KEY = 16777237
    SCROLL_DOWN_KEY = 16777235

    def __init__(self, scene, iface, parent=None, central_wgt=False):
        super().__init__(parent)

        self.iface = iface

        self.setScene(scene)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.central_wgt = central_wgt
        if not central_wgt:
            self.fit_in_view()

        self.scene_size = self.scene().sceneRect().size().toSize()
        self.sc = 1

        self.setMaximumSize(self.scene_size)
        self.resize(self.scene_size)
    
    def resize_event(self):
        sc_h = self.height() / self.scene_size.height()   
        sc_w = self.width() / self.scene_size.width()
        self.sc = min(sc_h, sc_w)

        self.fitInView(0, 0, self.width(), self.height(), Qt.KeepAspectRatio)
        self.scale(self.sc, self.sc)
        self.resize(self.scene_size.width() * self.sc, self.scene_size.height() * self.sc)

        d_w = self.parent().width() - self.width()
        d_w -= self.parent().page_selector.width()

        d_h = self.parent().height() - self.height()
        toolbar = self.parent().toolbar
        if toolbar.isHidden():
            d_h -= toolbar.height()

        self.setGeometry(d_w / 2, d_h / 2, self.width(), self.height())

    def resizeEvent(self, event):
        if self.central_wgt:
            self.resize_event()

        return super().resizeEvent(event)
    
    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() in [self.SCROLL_UP_KEY, self.SCROLL_DOWN_KEY]:
            self.iface.signalize_scroll(event)
        return super().keyPressEvent(event)

    def resize_wgt(self, width, height):
        self.resize(width, height)
        self.fit_in_view()
    
    def fit_in_view(self):
        w = self.scene().width()
        h = self.scene().height()
        
        width = self.width()
        space_w = 0.11
        space_h = 0.13
        if width < 220:
            space_w = 0.07
            space_h = 0.09
            
        self.setSceneRect(w/2 - w * space_w, h/2 - h * space_h, width, self.height())
        self.fitInView(0, 0, w, h, Qt.KeepAspectRatio)
    
    def mousePressEvent(self, event: QMouseEvent) -> None:
        if self.central_wgt:
            self.iface.signalize_page_clicked(event)
        return super().mousePressEvent(event)
        
