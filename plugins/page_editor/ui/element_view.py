from abc import ABC, abstractmethod
from PySide6.QtWidgets import QGraphicsItem


class ElementView(ABC):
    @abstractmethod
    def get_data(self):
        ...

    @abstractmethod
    def set_data(self, data):
        ...

    def new_position(self, x, y, bounding_rect, scene):
        scene_width = scene.width()
        scene_height = scene.height()

        if x < 0:
            x = 0
        elif x + bounding_rect.right() > scene_width:
            x = scene_width - bounding_rect.width()

        if y < 0:
            y = 0
        elif y + bounding_rect.bottom() > scene_height:
            y = scene_height - bounding_rect.height()

        return x, y

class ElementViewMeta(type(QGraphicsItem), type(ElementView)):
    ...