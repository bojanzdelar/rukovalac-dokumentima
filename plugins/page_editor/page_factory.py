from .model.page_handler import PageHandler
from .ui.page_scene import PageScene
from .ui.page_view import PageView


class PageFactory:
    def __init__(self, iface):
        self.iface = iface

    def create_handler(self, path, parent_path, format="PG") -> PageHandler:
        return PageHandler(path, parent_path, self.iface, format)

    def create_view(self, handler, parent=None, central_wgt=False, gen_view_scene=None) -> PageView:
        scene = PageScene(handler, parent, self.iface, gen_view_scene)
        return PageView(scene, self.iface, parent, central_wgt)