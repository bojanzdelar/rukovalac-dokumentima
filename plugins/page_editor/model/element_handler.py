import itertools
from abc import abstractmethod
from zipfile import ZIP_DEFLATED
from mutablezip import MutableZipFile
from core.model.abstract_handler import AbstractHandler


class ElementHandler(AbstractHandler):
    def __init__(self, path, parent_path, format=None):
        super().__init__(path, format)

        self.parent_path = parent_path

    @property
    @abstractmethod
    def asset_prefix(self):
        ...

    @property
    def asset_postfix(self):
        return f".{self.format.lower()}"

    def generate_asset_path(self, existing_paths):
        for i in itertools.count(start=1):
            path = f"{self.asset_prefix}{i}{self.asset_postfix}"
            if path not in existing_paths:
                self.path = path
                return

    def delete_file(self):
        with MutableZipFile(self.parent_path, "a", compression=ZIP_DEFLATED) as z:
            z.removeFile(self.path)
