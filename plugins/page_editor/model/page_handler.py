import json
from copy import copy
from zipfile import ZIP_DEFLATED
from mutablezip import MutableZipFile
from .element_handler import ElementHandler

class PageHandler(ElementHandler):
    def __init__(self, path, parent_path, iface, format="PG"):
        super().__init__(path, parent_path, format)

        self.data = []
        self.iface = iface

    @property
    def asset_prefix(self):
        return "pages/page"

    def read_file(self):
        with MutableZipFile(self.parent_path, "r") as z:
            with z.open(self.path, "r") as file:
                try:
                    self.data = json.loads(file.read().decode("utf-8"))
                except json.JSONDecodeError:
                    self.data = []

    def save_file(self, items, existing_paths):
        old_data = self.data
        self.data = list(map(self.convert_to_json, items)) 
       
        self._delete_elements(old_data)
        self._save_elements(items, existing_paths, old_data)    

        with MutableZipFile(self.parent_path, "a", compression=ZIP_DEFLATED) as z:
            z.writestr(self.path, json.dumps(self.data, indent=4).encode("utf-8"))

    def _remove_element_dimensions(self, el):
        new_el = copy(el)
        new_el.pop("width", None)
        new_el.pop("height", None)
        return new_el
    
    def _delete_elements(self, old_data):
        for element in old_data:
            # TODO: find more elegant solution?
            
            # remove dimension keys from dictionary because old_data list doesnt have that keys
            # and therefore their elements would never be equal
            data_without_dimensions = list(map(self._remove_element_dimensions, self.data))

            # check whether element from old data still exists
            # if it does, skip it
            if element in data_without_dimensions:
                continue

            self.iface.delete_element(element["type"], element["path"], self.parent_path)

    def _save_elements(self, items, existing_paths, old_data):
        for element, item in zip(self.data, items):
            path = self.iface.save_element(element["type"], item, element["path"], self.parent_path, existing_paths)
            
            if path:
                element["path"] = path

                if path not in existing_paths:
                    existing_paths.append(path)
            elif self._remove_element_dimensions(element) not in old_data:
                self.data.remove(element)

    def delete_file(self):
        super().delete_file()

        for element in self.data:
            if not element["path"]:
                continue

            self.iface.delete_element(element["type"], element["path"], self.parent_path)

    # TODO: move to class GraphicsItem with onMouseEvent method, self.path attribute
    def convert_to_json(self, item):
        return {
            "type": item.data_type,
            "path": item.path,
            "x": item.x(),
            "y": item.y(),
            "width": item.boundingRect().width(),
            "height": item.boundingRect().height()
        }