from abc import ABC, abstractmethod
from PySide6.QtGui import QMouseEvent
from ..ui.element_view import ElementView

class ElementEditorIn(ABC):
    @abstractmethod
    def add_action(self) -> None:
        ...

    @abstractmethod
    def insert_element(self, mouse_event: QMouseEvent) -> None:
        ...

    def create_element(self, path: str, document_path: str) -> ElementView:
        handler = self.factory.create_handler(path, document_path)
        handler.read_file()
        return self.factory.create_view(handler.data, path, None)
    
    def clone_element(self, element: ElementView) -> ElementView:
        return self.factory.create_view(element.get_data(), element.path, None)

    def save_element(self, element: ElementView, path: str, document_path: str, existing_paths: list) -> str:
        handler = self.factory.create_handler(path, document_path)

        pref = str(handler.asset_prefix)
        postf = str(handler.asset_postfix)
        if (not path) or (path[0:len(pref)] != pref) or (path[-len(postf):] != postf):
            handler.generate_asset_path(existing_paths)

        element.path = handler.path
        handler.save_file(element)

        return handler.path

    def delete_element(self, path: str, document_path: str) -> None:
        handler = self.factory.create_handler(path, document_path)
        handler.delete_file()