from abc import ABC, abstractmethod
from xmlrpc.client import Boolean

from plugins.page_editor.ui.element_view import ElementView
from ..model.page_handler import PageHandler
from ..ui.page_view import PageView

class PageEditorIn(ABC):
    @abstractmethod
    def add_element(self, element: ElementView, x: float, y: float) -> Boolean:
        ...

    @abstractmethod
    def create_handler(self, path, parent_path, format="PG") -> PageHandler:
        ...

    @abstractmethod
    def create_view(self, handler, parent=None, central_wgt=False, gen_view_scene=None) -> PageView:
        ...