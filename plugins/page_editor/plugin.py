import imp
from xmlrpc.client import Boolean
from plugin_manager.model.extension import Extension
from .interface.page_editor_in import PageEditorIn
from .page_factory import PageFactory
from .model.page_handler import PageHandler
from .ui.page_view import PageView
from .ui.element_view import ElementView

class Plugin(Extension, PageEditorIn):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        # FIXME: probably wont be needed
        self.factory = PageFactory(iface)
        self.is_active = False

    def activate(self):
        self.is_active = True
        self.iface.add_page_editor(self)

    def deactivate(self):
        self.is_active = False
        self.iface.remove_page_editor()

    def add_element(self, element: ElementView, x: float, y: float) -> Boolean:
        view = self.iface.get_current_page()
        scene = view.scene()
        return scene.add_item(element, x, y, view.sc)

    def create_handler(self, path, parent_path, format="PG") -> PageHandler:
        return self.factory.create_handler(path, parent_path, format)

    def create_view(self, handler, parent=None, central_wgt=False, gen_view_scene=None) -> PageView:
        return self.factory.create_view(handler, parent, central_wgt, gen_view_scene)
    
