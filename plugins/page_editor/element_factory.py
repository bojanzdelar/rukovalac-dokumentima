from abc import ABC, abstractmethod
from .model.element_handler import ElementHandler
from .ui.element_view import ElementView


class ElementFactory(ABC):
    @abstractmethod
    def create_handler(self, path: str, parent_path: str) -> ElementHandler:
        ...

    @abstractmethod
    def create_view(self, data, path: str, parent_path: str = None) -> ElementView:
        ...