from PySide6.QtWidgets import QFileDialog, QMessageBox
from PySide6.QtGui import QMouseEvent
from core.service.file_system_service import FileSystemService
from plugin_manager.model.extension import Extension
from plugins.page_editor.interface.element_editor_in import ElementEditorIn
from .image_factory import ImageFactory
from .model.image_handler import ImageHandler


class Plugin(Extension, ElementEditorIn):

    ELEMENT_TYPE = "image"
    ELEMENT_ACTION = "insert-image"

    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.factory = ImageFactory()
        self.item = None
        self.is_active = False

    def activate(self):
        self.is_active = True
        self.iface.add_element_editor(self.ELEMENT_TYPE, self)
        self.add_action()

    def deactivate(self):
        self.is_active = False
        self.iface.remove_element_editor(self.ELEMENT_TYPE)
        self.iface.remove_document_action(self.ELEMENT_ACTION)

    def add_action(self):
        if not self.is_active:
            return

        self.iface.add_document_action(self.ELEMENT_ACTION, "plugins/image_viewer/resources/icons/insert-image.png",
                self.choose_image, "", "Insert image")

    def choose_image(self):
        if not self.is_active or not self.iface.is_action_checked(self.ELEMENT_ACTION):
            return

        dir = FileSystemService.get_instance().get_root_directory()
        file, _ = QFileDialog.getOpenFileName(
                self.iface.mw, "Select image", dir, "Image Files (*.bmp *.gif *.jpg *.jpeg *.png)")
        
        if not file:
            self.iface.set_action_checked(self.ELEMENT_ACTION, False)
            return
        
        self.item = self.factory.create_view(ImageHandler.generate_pixmap(file), file)

    def insert_element(self, mouse_event: QMouseEvent):
        if not self.is_active or not self.item or not self.iface.is_action_checked(self.ELEMENT_ACTION):
            return
            
        added = self.iface.request_insert_element(self.item, mouse_event.x(), mouse_event.y())

        if added:
            self.iface.set_action_checked(self.ELEMENT_ACTION, False)
        else:
            QMessageBox.warning(self.iface.mw, "Error", "You aren't allowed to add new element on top of existing ones!")