from plugins.page_editor.element_factory import ElementFactory
from .model.image_handler import ImageHandler
from .ui.image_view import ImageView


class ImageFactory(ElementFactory):
    def create_handler(self, path, parent_path) -> ImageHandler:
        return ImageHandler(path, parent_path)

    def create_view(self, pixmap, path, parent=None) -> ImageView:
        return ImageView(pixmap, path, parent)