from PySide6.QtCore import Qt
from PySide6.QtWidgets import QGraphicsItem, QGraphicsPixmapItem
from plugins.page_editor.ui.element_view import ElementView, ElementViewMeta
from plugins.page_editor.ui.scalable import Scalable

class ImageView(QGraphicsPixmapItem, ElementView, Scalable, metaclass=ElementViewMeta):
    data_type = "image"

    def __init__(self, pixmap, path, parent=None):
        super().__init__(pixmap, parent)
        self.path = path
        self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
        self.setTransformationMode(Qt.SmoothTransformation)

    def mouseMoveEvent(self, event):
        old_x, old_y = self.x(), self.y()

        super().mouseMoveEvent(event)

        self.setPos(*self.new_position(self.x(), self.y(), self.boundingRect(), self.scene()))

        if len(self.collidingItems()):
            self.setPos(old_x, old_y)
    
    def get_data(self):
        return self.pixmap().copy()
    
    def set_data(self, data):
        self.setPixmap(data)

    def scale_to_page(self, page_size, max_ratio):
        width_ratio = self.boundingRect().width() / page_size.width()
        height_ratio = self.boundingRect().height() / page_size.height()

        if width_ratio > max_ratio or height_ratio > max_ratio:
            data = self.get_data()

            if width_ratio > height_ratio:
                data = data.scaledToWidth(page_size.width() * max_ratio)
            else:
                data = data.scaledToHeight(page_size.height() * max_ratio)

            self.set_data(data)
