from zipfile import ZIP_DEFLATED
from mutablezip import MutableZipFile
from PySide6.QtCore import QBuffer, QByteArray, QIODevice
from PySide6.QtGui import QImage, QPixmap
from plugins.page_editor.model.element_handler import ElementHandler
from ..ui.image_view import ImageView


class ImageHandler(ElementHandler):
    def __init__(self, path, parent_path, format="JPG"):
        super().__init__(path, parent_path, format)

    @staticmethod
    def generate_pixmap(path: str) -> QPixmap:
        image = QImage(path)
        return QPixmap.fromImage(image)

    @property
    def asset_prefix(self):
        return "assets/image/image"

    def read_file(self):
        with MutableZipFile(self.parent_path, "r") as z:
            try:
                with z.open(self.path, "r") as file:
                    image = QImage.fromData(file.read())
                    self.data = QPixmap.fromImage(image)
            except FileNotFoundError:
                self.data = ""

    def save_file(self, image_view: ImageView):
        with MutableZipFile(self.parent_path, "a", compression=ZIP_DEFLATED) as z:
            pixmap = image_view.pixmap()
            ba = QByteArray()
            buff = QBuffer(ba)
            buff.open(QIODevice.WriteOnly) 
            pixmap.save(buff, "JPG")
            pixmap_bytes = ba.data()
            z.writestr(self.path, pixmap_bytes)
